import 'package:flutter/cupertino.dart';
import 'package:rdcloseoutsession/ApiProvider.dart';
import 'package:rdcloseoutsession/list_engagement.dart';

class ListEngagementRepository {
  ApiProvider _provider = ApiProvider();

  String baseUrl =
      'https://avexisstaging.radiusdirect.net/coreapi/v2/listEngagement';

  Future<ListEngagement> fetchChuckCategoryData(
      String enggType,
      String startTime,
      String endTime,
      String limit,
      int page,
      String filterRep,
      String dashboard, ValueChanged<ListEngagement> onupdatelistiner) async {
//    if (enggType == "Today") {
//      final response = await _provider.callNetworkRequest(url, bodyFuture, 1);
//      return ListEngagement.fromMap(response);
//    } else {
//      final response = await _provider.callNetworkRequest(url, bodyToday, 1);
//      return ListEngagement.fromMap(response);
//    }
    startTime = '2020-03-01T18:30:00.000Z';
    endTime = '2020-04-05T18:29:00.000Z';

    Map<String, String> bodyToday = {
//    'start_time': '2020-02-09T18:30:00.000Z',
//    'limit': '10',
//    'end_time': '2020-02-22T18:29:00.000Z',
//    'page': '1',
//    'filter_with_rep': '0',
//    'dashboard': '0',

      'start_time': startTime,
      'limit': limit,
      'end_time': endTime,
      'page': page.toString(),
      'type': enggType,
      'filter_with_rep': filterRep,
      'dashboard': dashboard,
    };

    Map<String, String> bodyFuture = {
//    'start_time': '2020-02-09T18:30:00.000Z',
//    'limit': '10',
//    'end_time': '2020-02-22T18:29:00.000Z',
//    'page': '1',
//    'filter_with_rep': '0',
//    'dashboard': '0',

      'start_time': startTime,
      'limit': limit,
      'end_time': endTime,
      'page': page.toString(),
      'type': enggType,
      'filter_with_rep': filterRep,
      'dashboard': dashboard,
    };

    print('check params future >> $bodyFuture');
    print('base utllll >> $baseUrl');
    print('check params today >> $bodyToday');

    String url = baseUrl;
    final response = await _provider.callNetworkRequest(
        url, enggType == "Today" ? bodyToday : bodyFuture, 1);
    if(onupdatelistiner!=null) {
      onupdatelistiner(ListEngagement.fromMap(response));
    }
    return ListEngagement.fromMap(response);
  }
}
