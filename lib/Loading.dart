import 'package:flutter/material.dart';

class Loading extends StatelessWidget {
  final String loadingMessage;
  final bool isShowLoader;

  const Loading({Key key, this.loadingMessage, this.isShowLoader})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent, // background color
      // makes widget fullscreen
      // child: Material(
      body: SafeArea(
        child: Visibility(
          visible: false,
          child: Center(
            child: Container(
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Color(0xFFffffff),
                border: Border.all(
                  color: Color(0xFFffffff),
                ),
                borderRadius: BorderRadius.circular(100),
              ),
              child: SizedBox(
                height: 18.0,
                width: 18.0,
                child: CircularProgressIndicator(
                  strokeWidth: 2.5,
                  valueColor: new AlwaysStoppedAnimation<Color>(
                      Theme.of(context).primaryColor),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
