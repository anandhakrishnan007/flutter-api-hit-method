import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:rdcloseoutsession/Loading.dart';
import 'package:rdcloseoutsession/Response.dart';
import 'package:rdcloseoutsession/base.dart';
import 'package:rdcloseoutsession/bread_crumb_container.dart';
import 'package:rdcloseoutsession/custom_app_theme.dart';
import 'package:rdcloseoutsession/custom_dimen.dart';
import 'package:rdcloseoutsession/empty_message_container.dart';
import 'package:rdcloseoutsession/engagement.dart';
import 'package:rdcloseoutsession/engagement_cardcontainer.dart';
import 'package:rdcloseoutsession/incrementally_loading_listview.dart';
import 'package:rdcloseoutsession/list_engagement.dart';
import 'package:rdcloseoutsession/network/ListEngagementBloc.dart';
import 'package:rdcloseoutsession/resumable_state.dart';
import 'package:rdcloseoutsession/tab_item_container.dart';

enum TabItem { Today, Future }

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends ResumableState<HomePage> with BasePage {
  final GlobalKey<RefreshIndicatorState> _scaffoldKey =
      new GlobalKey<RefreshIndicatorState>();
  String todayCount = '0', futureCount = '0';
  String todayBubbleCount = '!', futureBubbleCount = '!';

  bool isSelectedTabItemToday = true;
  bool isSelectedTabItemFuture = false;
  bool _saleTeamEnable = false;

  TabItem tabItems = TabItem.Today;
  List<Engagement> engagementList;
  bool filtered = false;

  //31-03-20-RK
  List<ListEngagement> listengagementdata;
  ListEngagement listEngagement;

  String postType = 'Today';

  bool loaderStatus = true;
  ListEngagementBloc _bloc;

  int page = 1;
  String limit = '5';
  String startTime = '';
  String endTime = '';
  String dashBoard = '1';
  String filterRep = '0';

  // RK
  bool _loadingMore;
  bool _hasMoreItems = true;
  int _maxItems = 30;
  int _numItemsPage = 10;
  Future _initialLoad;

  @override
  void initState() {
    // TODO: implement initState
//    tagInStateWithLifecycle = "_TestRouteState";
    super.initState();
    engagementList = new List();

//    homeFilterDomain = HomeFilterDomain.getInitialize();

//    engagementList = [
//      Engagement(
//          '100',
//          'Live Engagement 2020',
//          0xFF424242,
//          'Owner',
//          'Awaiting Speaker Reply',
//          'Introduction to ZOLGENSMA (onasemnogene '
//              'abeparvovec-xioi) suspension for intravenous infusion',
//          'ZOLGENSMA&nbsp;<a class=\"isitext\" href='
//              '\"https://www.zolgensma.com/\" target=\"_blank\">ISI</a>',
//          'Avexis',
//          'Prethivraj',
//          'Arun A',
//          '6',
//          '9',
//          'Nov 18, 1990',
//          '12:00 PM'),
//      Engagement(
//          '101',
//          'Live in office',
//          0xFF424242,
//          'Collaborate',
//          'Pending Venue Confirmation',
//          'Mechanism of Action',
//          'ZOLGENSMA&nbsp;<a class=\"isitext\" href='
//              '\"https://www.zolgensma.com/\" target=\"_blank\">ISI</a>',
//          'AveXis',
//          'Prethivraj',
//          'Haasini M',
//          '56',
//          '100',
//          'Jan 21, 2020',
//          '03:06 PM'),
//      Engagement(
//          '100',
//          'Live out of office',
//          0xFF424242,
//          'Owner',
//          'Completed',
//          'Introduction to ZOLGENSMA (onasemnogene '
//              'abeparvovec-xioi) suspension for intravenous infusion',
//          'ZOLGENSMA&nbsp;<a class=\"isitext\" href='
//              '\"https://www.zolgensma.com/\" target=\"_blank\">ISI</a>',
//          'AveXis',
//          'Prethivraj',
//          'Arun A',
//          '6',
//          '9',
//          'Nov 18, 1990',
//          '12:00 PM'),
//      Engagement(
//          '101',
//          'Live in office',
//          0xFF424242,
//          'Collaborate',
//          'Pending Venue Confirmation',
//          'Introduction to ZOLGENSMA (onasemnogene '
//              'abeparvovec-xioi) suspension for intravenous infusion',
//          'ZOLGENSMA&nbsp;<a class=\"isitext\" href='
//              '\"https://www.zolgensma.com/\" target=\"_blank\">ISI</a>',
//          'AveXis',
//          'Prethivraj',
//          'Haasini M',
//          '56',
//          '100',
//          'Jan 21, 2020',
//          '03:06 PM'),
//      Engagement(
//          '100',
//          'Live Engagement 2020',
//          0xFF424242,
//          'Owner',
//          'Awaiting Speaker Reply',
//          'Introduction to ZOLGENSMA (onasemnogene '
//              'abeparvovec-xioi) suspension for intravenous infusion',
//          'ZOLGENSMA&nbsp;<a class=\"isitext\" href='
//              '\"https://www.zolgensma.com/\" target=\"_blank\">ISI</a>',
//          'Avexis',
//          'Prethivraj',
//          'Arun A',
//          '6',
//          '9',
//          'Nov 18, 1990',
//          '12:00 PM'),
//      Engagement(
//          '101',
//          'Live in office',
//          0xFF424242,
//          'Collaborate',
//          'Pending Venue Confirmation',
//          'Mechanism of Action',
//          'ZOLGENSMA&nbsp;<a class=\"isitext\" href='
//              '\"https://www.zolgensma.com/\" target=\"_blank\">ISI</a>',
//          'AveXis',
//          'Prethivraj',
//          'Haasini M',
//          '56',
//          '100',
//          'Jan 21, 2020',
//          '03:06 PM'),
//      Engagement(
//          '100',
//          'Live out of office',
//          0xFF424242,
//          'Owner',
//          'Completed',
//          'Introduction to ZOLGENSMA (onasemnogene '
//              'abeparvovec-xioi) suspension for intravenous infusion',
//          'ZOLGENSMA&nbsp;<a class=\"isitext\" href='
//              '\"https://www.zolgensma.com/\" target=\"_blank\">ISI</a>',
//          'AveXis',
//          'Prethivraj',
//          'Arun A',
//          '6',
//          '9',
//          'Nov 18, 1990',
//          '12:00 PM'),
//      Engagement(
//          '101',
//          'Live in office',
//          0xFF424242,
//          'Collaborate',
//          'Pending Venue Confirmation',
//          'Introduction to ZOLGENSMA (onasemnogene '
//              'abeparvovec-xioi) suspension for intravenous infusion',
//          'ZOLGENSMA&nbsp;<a class=\"isitext\" href='
//              '\"https://www.zolgensma.com/\" target=\"_blank\">ISI</a>',
//          'AveXis',
//          'Prethivraj',
//          'Haasini M',
//          '56',
//          '100',
//          'Jan 21, 2020',
//          '03:06 PM'),
//      Engagement(
//          '100',
//          'Live Engagement 2020',
//          0xFF424242,
//          'Owner',
//          'Awaiting Speaker Reply',
//          'Introduction to ZOLGENSMA (onasemnogene '
//              'abeparvovec-xioi) suspension for intravenous infusion',
//          'ZOLGENSMA&nbsp;<a class=\"isitext\" href='
//              '\"https://www.zolgensma.com/\" target=\"_blank\">ISI</a>',
//          'Avexis',
//          'Prethivraj',
//          'Arun A',
//          '6',
//          '9',
//          'Nov 18, 1990',
//          '12:00 PM'),
//      Engagement(
//          '101',
//          'Live in office',
//          0xFF424242,
//          'Collaborate',
//          'Pending Venue Confirmation',
//          'Mechanism of Action',
//          'ZOLGENSMA&nbsp;<a class=\"isitext\" href='
//              '\"https://www.zolgensma.com/\" target=\"_blank\">ISI</a>',
//          'AveXis',
//          'Prethivraj',
//          'Haasini M',
//          '56',
//          '100',
//          'Jan 21, 2020',
//          '03:06 PM'),
//      Engagement(
//          '100',
//          'Live out of office',
//          0xFF424242,
//          'Owner',
//          'Completed',
//          'Introduction to ZOLGENSMA (onasemnogene '
//              'abeparvovec-xioi) suspension for intravenous infusion',
//          'ZOLGENSMA&nbsp;<a class=\"isitext\" href='
//              '\"https://www.zolgensma.com/\" target=\"_blank\">ISI</a>',
//          'AveXis',
//          'Prethivraj',
//          'Arun A',
//          '6',
//          '9',
//          'Nov 18, 1990',
//          '12:00 PM'),
//      Engagement(
//          '101',
//          'Live in office',
//          0xFF424242,
//          'Collaborate',
//          'Pending Venue Confirmation',
//          'Introduction to ZOLGENSMA (onasemnogene '
//              'abeparvovec-xioi) suspension for intravenous infusion',
//          'ZOLGENSMA&nbsp;<a class=\"isitext\" href='
//              '\"https://www.zolgensma.com/\" target=\"_blank\">ISI</a>',
//          'AveXis',
//          'Prethivraj',
//          'Haasini M',
//          '56',
//          '100',
//          'Jan 21, 2020',
//          '03:06 PM'),
//      Engagement(
//          '100',
//          'Live Engagement 2020',
//          0xFF424242,
//          'Owner',
//          'Awaiting Speaker Reply',
//          'Introduction to ZOLGENSMA (onasemnogene '
//              'abeparvovec-xioi) suspension for intravenous infusion',
//          'ZOLGENSMA&nbsp;<a class=\"isitext\" href='
//              '\"https://www.zolgensma.com/\" target=\"_blank\">ISI</a>',
//          'Avexis',
//          'Prethivraj',
//          'Arun A',
//          '6',
//          '9',
//          'Nov 18, 1990',
//          '12:00 PM'),
//      Engagement(
//          '101',
//          'Live in office',
//          0xFF424242,
//          'Collaborate',
//          'Pending Venue Confirmation',
//          'Mechanism of Action',
//          'ZOLGENSMA&nbsp;<a class=\"isitext\" href='
//              '\"https://www.zolgensma.com/\" target=\"_blank\">ISI</a>',
//          'AveXis',
//          'Prethivraj',
//          'Haasini M',
//          '56',
//          '100',
//          'Jan 21, 2020',
//          '03:06 PM'),
//      Engagement(
//          '100',
//          'Live out of office',
//          0xFF424242,
//          'Owner',
//          'Completed',
//          'Introduction to ZOLGENSMA (onasemnogene '
//              'abeparvovec-xioi) suspension for intravenous infusion',
//          'ZOLGENSMA&nbsp;<a class=\"isitext\" href='
//              '\"https://www.zolgensma.com/\" target=\"_blank\">ISI</a>',
//          'AveXis',
//          'Prethivraj',
//          'Arun A',
//          '6',
//          '9',
//          'Nov 18, 1990',
//          '12:00 PM'),
//      Engagement(
//          '101',
//          'Live in office',
//          0xFF424242,
//          'Collaborate',
//          'Pending Venue Confirmation',
//          'Introduction to ZOLGENSMA (onasemnogene '
//              'abeparvovec-xioi) suspension for intravenous infusion',
//          'ZOLGENSMA&nbsp;<a class=\"isitext\" href='
//              '\"https://www.zolgensma.com/\" target=\"_blank\">ISI</a>',
//          'AveXis',
//          'Prethivraj',
//          'Haasini M',
//          '56',
//          '100',
//          'Jan 21, 2020',
//          '03:06 PM'),
//      Engagement(
//          '100',
//          'Live Engagement 2020',
//          0xFF424242,
//          'Owner',
//          'Awaiting Speaker Reply',
//          'Introduction to ZOLGENSMA (onasemnogene '
//              'abeparvovec-xioi) suspension for intravenous infusion',
//          'ZOLGENSMA&nbsp;<a class=\"isitext\" href='
//              '\"https://www.zolgensma.com/\" target=\"_blank\">ISI</a>',
//          'Avexis',
//          'Prethivraj',
//          'Arun A',
//          '6',
//          '9',
//          'Nov 18, 1990',
//          '12:00 PM'),
//      Engagement(
//          '101',
//          'Live in office',
//          0xFF424242,
//          'Collaborate',
//          'Pending Venue Confirmation',
//          'Mechanism of Action',
//          'ZOLGENSMA&nbsp;<a class=\"isitext\" href='
//              '\"https://www.zolgensma.com/\" target=\"_blank\">ISI</a>',
//          'AveXis',
//          'Prethivraj',
//          'Haasini M',
//          '56',
//          '100',
//          'Jan 21, 2020',
//          '03:06 PM'),
//      Engagement(
//          '100',
//          'Live out of office',
//          0xFF424242,
//          'Owner',
//          'Completed',
//          'Introduction to ZOLGENSMA (onasemnogene '
//              'abeparvovec-xioi) suspension for intravenous infusion',
//          'ZOLGENSMA&nbsp;<a class=\"isitext\" href='
//              '\"https://www.zolgensma.com/\" target=\"_blank\">ISI</a>',
//          'AveXis',
//          'Prethivraj',
//          'Arun A',
//          '6',
//          '9',
//          'Nov 18, 1990',
//          '12:00 PM'),
//      Engagement(
//          '101',
//          'Live in office',
//          0xFF424242,
//          'Collaborate',
//          'Pending Venue Confirmation',
//          'Introduction to ZOLGENSMA (onasemnogene '
//              'abeparvovec-xioi) suspension for intravenous infusion',
//          'ZOLGENSMA&nbsp;<a class=\"isitext\" href='
//              '\"https://www.zolgensma.com/\" target=\"_blank\">ISI</a>',
//          'AveXis',
//          'Prethivraj',
//          'Haasini M',
//          '56',
//          '100',
//          'Jan 21, 2020',
//          '03:06 PM'),
//      Engagement(
//          '100',
//          'Live Engagement 2020',
//          0xFF424242,
//          'Owner',
//          'Awaiting Speaker Reply',
//          'Introduction to ZOLGENSMA (onasemnogene '
//              'abeparvovec-xioi) suspension for intravenous infusion',
//          'ZOLGENSMA&nbsp;<a class=\"isitext\" href='
//              '\"https://www.zolgensma.com/\" target=\"_blank\">ISI</a>',
//          'Avexis',
//          'Prethivraj',
//          'Arun A',
//          '6',
//          '9',
//          'Nov 18, 1990',
//          '12:00 PM'),
//      Engagement(
//          '101',
//          'Live in office',
//          0xFF424242,
//          'Collaborate',
//          'Pending Venue Confirmation',
//          'Mechanism of Action',
//          'ZOLGENSMA&nbsp;<a class=\"isitext\" href='
//              '\"https://www.zolgensma.com/\" target=\"_blank\">ISI</a>',
//          'AveXis',
//          'Prethivraj',
//          'Haasini M',
//          '56',
//          '100',
//          'Jan 21, 2020',
//          '03:06 PM'),
//      Engagement(
//          '100',
//          'Live out of office',
//          0xFF424242,
//          'Owner',
//          'Completed',
//          'Introduction to ZOLGENSMA (onasemnogene '
//              'abeparvovec-xioi) suspension for intravenous infusion',
//          'ZOLGENSMA&nbsp;<a class=\"isitext\" href='
//              '\"https://www.zolgensma.com/\" target=\"_blank\">ISI</a>',
//          'AveXis',
//          'Prethivraj',
//          'Arun A',
//          '6',
//          '9',
//          'Nov 18, 1990',
//          '12:00 PM'),
//      Engagement(
//          '101',
//          'Live in office',
//          0xFF424242,
//          'Collaborate',
//          'Pending Venue Confirmation',
//          'Introduction to ZOLGENSMA (onasemnogene '
//              'abeparvovec-xioi) suspension for intravenous infusion',
//          'ZOLGENSMA&nbsp;<a class=\"isitext\" href='
//              '\"https://www.zolgensma.com/\" target=\"_blank\">ISI</a>',
//          'AveXis',
//          'Prethivraj',
//          'Haasini M',
//          '56',
//          '100',
//          'Jan 21, 2020',
//          '03:06 PM'),
//    ];

    //============= Start ==========
    listengagementdata = new List();
    dateTimeFormat();
    _bloc = ListEngagementBloc(
        startTime, endTime, page, limit, dashBoard, filterRep,null);

    WidgetsBinding.instance
        .addPostFrameCallback((_) => _scaffoldKey.currentState.show());

    //============= End ===========
  }

  Future _loadMoreItems(int lengthData) async {
    final totalItems = lengthData;
    await Future.delayed(Duration(seconds: 3), () {
//      for (var i = 0; i < _numItemsPage; i++) {
//        lengthData.add(Item('Item ${totalItems + i + 1}'));
//      }
    });

    _hasMoreItems = lengthData < _maxItems;
  }

  void dateTimeFormat() {
    print('enter the datetime format ');
    var oldformat = DateFormat("MMMM dd,yyyy", 'en_US');

    var strtformat = DateFormat("MMMM dd,yyyy HH:mm", 'en_US');
    var newformat = DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", 'en_US');

    try {
      DateTime date = new DateTime.now();
      String sdat = oldformat.format(date);

      DateTime startd = strtformat.parse(sdat + " 00:00");

      DateTime endd = strtformat.parse(sdat + " 23:59");

      String startdate = newformat.format(startd.toUtc());
      String enddate = newformat.format(endd.toUtc());

      startTime = startdate;
      endTime = enddate;
    } catch (e) {
      e.printStackTrace();
    }
  }

  @override
  void onReady() {
    // Implement your code inside here
//    print('HomePage is ready!');
  }

  @override
  void onResume() {
    // Implement your code inside here
//    print('HomePage is resumed!');
  }

  @override
  void onPause() {
    // Implement your code inside here
//    print('HomePage is paused!');
  }

  @override
  void dispose() {
    super.dispose();

//    print('HomePage is dispose!');
  }

  @override
  Widget build(BuildContext context) {
    getParameter(context);

//    AuthProvider authProvider = Provider.of<AuthProvider>(context);

    return Container(
//      key: _scaffoldKey,
      color: CustomAppTheme.dividerColor,
      child: Column(
        children: <Widget>[
          BreadCrumbContainer(
            filtered: filtered,
            isTabletDevice: isTabletDevice,
            filterOnTap: () {
//              push(
//                context,
//                MaterialPageRoute(
//                  builder: (context) => FilterPage(
//                    FilterPageType.Home,
//                    true,
//                  ),
//                ),
//              );
            },
          ),
//          Visibility(
//            visible: authProvider?.user?.user?.userType != null &&
//                authProvider?.user?.user?.userType != '' &&
//                authProvider?.user?.user?.userType ==
//                    StringConstants.repUserType,
//            child: ToggleSwitchContainer(
//              title: StringConstants.viewSalesTeam,
//              selected: _saleTeamEnable,
//              onItemChanged: (bool value) {
//                setState(() {
//                  _saleTeamEnable = value;
//                  print('Sales Team --> $_saleTeamEnable');
//                });
//              },
//            ),
//          ),
          AnimatedContainer(
            duration: Duration(seconds: 1),
            width: MediaQuery.of(context).size.width,
            height: isTabletDevice ? 90.0 : CustomDimension.tabContainerHeight,
            child: Stack(
              children: <Widget>[
                Container(
                  color: Colors.white,
                  height: isTabletDevice
                      ? 90.0 / 2
                      : CustomDimension.tabContainerHeight / 2,
                ),
                TabAlignContainer(
                  alignment: Alignment.centerLeft,
                  isSelected: tabItems == TabItem.Today,
                  tabItem: TabItem.Today,
                ),
                TabAlignContainer(
                  alignment: Alignment.centerRight,
                  isSelected: tabItems == TabItem.Future,
                  tabItem: TabItem.Future,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
                  child: Container(
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Stack(
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width,
                                decoration: tabItems == TabItem.Today
                                    ? BoxDecoration(
                                        color: Color(0xFFe4e7f0),
                                        borderRadius: BorderRadius.only(
                                            bottomRight: Radius.circular(5.0),
                                            topRight: Radius.circular(5.0),
                                            topLeft: Radius.circular(5.0)),
                                      )
                                    : BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.only(
                                            bottomRight: Radius.circular(5.0)),
                                      ),
                                child: InkWell(
                                  onTap: () {
                                    setState(() {
                                      print("click Today >>>");
                                      setState(() {
                                        postType = 'Today';
                                        dateTimeFormat();
                                      });
                                      _scaffoldKey.currentState.show();
                                      tabItems = TabItem.Today;
                                    });
                                  },
                                  child: TabItemContainer(
                                    title: 'Today',
                                    count: todayCount,
                                    isTablet: isTabletDevice,
                                    fontWeight: FontWeight.w400,
                                    colorCode: Color(0xFF04cfc7),
                                    isSelected: tabItems == TabItem.Today,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Stack(
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width,
                                decoration: tabItems == TabItem.Future
                                    ? BoxDecoration(
                                        color: Color(0xFFe4e7f0),
                                        borderRadius: BorderRadius.only(
                                            bottomRight: Radius.circular(5.0),
                                            topRight: Radius.circular(5.0),
                                            topLeft: Radius.circular(5.0)),
                                      )
                                    : BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.only(
                                            bottomLeft: Radius.circular(5.0)),
                                      ),
                                child: InkWell(
                                  onTap: () {
                                    setState(() {
                                      print("click Future >>>");
                                      setState(() {
                                        postType = 'Future';
                                      });
                                      _scaffoldKey.currentState.show();
                                      tabItems = TabItem.Future;
                                    });
                                  },
                                  child: TabItemContainer(
                                    title: 'Future',
                                    count: futureCount,
                                    isTablet: isTabletDevice,
                                    fontWeight: FontWeight.w400,
                                    colorCode: Color(0xFF04cfc7),
                                    isSelected: tabItems == TabItem.Future,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          Expanded(
            child: RefreshIndicator(
              key: _scaffoldKey,
              color: Theme.of(context).primaryColor,
              onRefresh: () => _bloc.fetchListEngagementData(postType,
                  startTime, endTime, page, limit, dashBoard, filterRep,null),
              child: StreamBuilder<Response<ListEngagement>>(
                  stream: _bloc.engagementListStream,
                  // ignore: missing_return
                  builder: (context, snapshot) {
                    if (snapshot != null && snapshot.hasData) {
//                      if (snapshot.data.data.totalCount.toString() != null &&
//                          snapshot.data.data.totalCount.toString() != '') {
//                        todayCount = snapshot.data.data.totalCount.toString();
//                      }
                      // ignore: missing_return
                      switch (snapshot.data.status) {
                        case Status.LOADING:
//                          CommonUtils.showLoader(context, 'engagement', false);
                          return Loading(
                            loadingMessage: snapshot.data.message,
                            isShowLoader: loaderStatus,
                          );
                          break;

                        case Status.COMPLETED:
                          if (snapshot.data.data == null ||
                              snapshot.data.data.result.length == 0) {
                            if (postType == 'Today') {
                              todayCount = '0';
                            } else {
                              futureCount = '0';
                            }
                          }

                          return snapshot.data.data == null ||
                                  snapshot.data.data.result.length == 0
                              ? Center(
                                  child: EmptyItemContainer(
                                    message: 'No engagements found',
                                    isTablet: isTabletDevice,
                                  ),
                                )
                              : IncrementallyLoadingListView(
                                  hasMore: () => _hasMoreItems,
                                  itemCount: () =>
                                      snapshot.data.data.result.length,
                                  // ignore: missing_return
                                  loadMore: () {
                                    // can shorten to "loadMore: _loadMoreItems" but this syntax is used to demonstrate that
                                    // functions with parameters can also be invoked if needed
//                                    await _loadMoreItems(
//                                        snapshot.data.data.result.length);
                                    print('enter the load moreeeee');
                                  },
                                  onLoadMore: () {
                                    setState(() {
                                      _loadingMore = true;
                                    });
                                  },
                                  onLoadMoreFinished: () {
                                    setState(() {
                                      _loadingMore = false;
                                    });
                                  },
                                  loadMoreOffsetFromBottom: 4,
                                  itemBuilder: (context, index) {
                                    final item =
                                        snapshot.data.data.result[index];
                                    if ((_loadingMore ?? false) &&
                                        index ==
                                            snapshot.data.data.result.length -
                                                1) {
                                      print('enter if condition');
                                      return Column(
                                        children: <Widget>[
                                          EngagementCardContainer(
                                            isTablet: isTabletDevice,
                                            isEngagement: true,
                                            engagement: snapshot
                                                .data.data.result[index],
                                          ),
                                        ],
                                      );
                                    }
                                    return EngagementCardContainer(
                                      isTablet: isTabletDevice,
                                      isEngagement: true,
                                      engagement:
                                          snapshot.data.data.result[index],
                                    );
                                  },
                                );
                          break;

                        case Status.ERROR:
                      }
                    }
                    return Container();
                  }),
            ),
          )
        ],
      ),
    );
  }
}

class TabAlignContainer extends StatelessWidget {
  final bool isSelected;
  final Alignment alignment;
  final TabItem tabItem;

  TabAlignContainer({this.isSelected, this.alignment, this.tabItem});

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: alignment,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: tabItem == TabItem.Today
              ? BorderRadius.only(
                  bottomRight: Radius.circular(isSelected ? 5 : 0),
                )
              : BorderRadius.only(
                  bottomLeft: Radius.circular(isSelected ? 5 : 0),
                ),
        ),
        width: 10.0,
      ),
    );
  }
}
