import 'dart:convert';

testmodel welcomeFromJson(String str) => testmodel.fromJson(json.decode(str));

String welcomeToJson(testmodel data) => json.encode(data.toJson());

class testmodel {
  int statusCode;
  bool apiStatus;
  int totalCount;
  List<Result> result;

  testmodel({
    this.statusCode,
    this.apiStatus,
    this.totalCount,
    this.result,
  });

  factory testmodel.fromJson(Map<String, dynamic> json) => testmodel(
    statusCode: json["statusCode"],
    apiStatus: json["apiStatus"],
    totalCount: json["totalCount"],
    result: List<Result>.from(json["result"].map((x) => Result.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "statusCode": statusCode,
    "apiStatus": apiStatus,
    "totalCount": totalCount,
    "result": List<dynamic>.from(result.map((x) => x.toJson())),
  };
}

class Result {
  EngagementType engagementType;
  int id;
  User user;
  String topic;
  DateTime startTime;
  String meetingId;
  Brand brand;
  ProductClass product;
  Location location;
  int status;
  int venueStatus;
  int isStart;
  String venueSelectedTimezone;
  String eoSelectedTimezone;
  String displayStatus;
  List<dynamic> primaryMeeting;
  List<Speaker> speaker;
  CollaborationMeeting collaborationMeeting;
  int invited;
  int confirmedInvitees;
  List<String> hcpOrRep;
  ParticipantStatus participantStatus;

  Result({
    this.engagementType,
    this.id,
    this.user,
    this.topic,
    this.startTime,
    this.meetingId,
    this.brand,
    this.product,
    this.location,
    this.status,
    this.venueStatus,
    this.isStart,
    this.venueSelectedTimezone,
    this.eoSelectedTimezone,
    this.displayStatus,
    this.primaryMeeting,
    this.speaker,
    this.collaborationMeeting,
    this.invited,
    this.confirmedInvitees,
    this.hcpOrRep,
    this.participantStatus,
  });

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    engagementType: EngagementType.fromJson(json["engagementType"]),
    id: json["id"],
    user: User.fromJson(json["user"]),
    topic: json["topic"],
    startTime: DateTime.parse(json["start_time"]),
    meetingId: json["meeting_id"],
    brand: Brand.fromJson(json["brand"]),
    product: ProductClass.fromJson(json["product"]),
    location: json["location"] == null ? null : Location.fromJson(json["location"]),
    status: json["status"],
    venueStatus: json["venue_status"],
    isStart: json["is_start"],
    venueSelectedTimezone: json["venue_selected_timezone"],
    eoSelectedTimezone: json["eo_selected_timezone"],
    displayStatus: json["displayStatus"],
    primaryMeeting: List<dynamic>.from(json["primaryMeeting"].map((x) => x)),
    speaker: List<Speaker>.from(json["speaker"].map((x) => speakerValues.map[x])),
    collaborationMeeting: collaborationMeetingValues.map[json["collaboration_meeting"]],
    invited: json["invited"],
    confirmedInvitees: json["confirmed_invitees"],
    hcpOrRep: List<String>.from(json["hcpOrRep"].map((x) => x)),
    participantStatus: ParticipantStatus.fromJson(json["participantStatus"]),
  );

  Map<String, dynamic> toJson() => {
    "engagementType": engagementType.toJson(),
    "id": id,
    "user": user.toJson(),
    "topic": topic,
    "start_time": startTime.toIso8601String(),
    "meeting_id": meetingId,
    "brand": brand.toJson(),
    "product": product.toJson(),
    "location": location == null ? null : location.toJson(),
    "status": status,
    "venue_status": venueStatus,
    "is_start": isStart,
    "venue_selected_timezone": venueSelectedTimezone,
    "eo_selected_timezone": eoSelectedTimezone,
    "displayStatus": displayStatus,
    "primaryMeeting": List<dynamic>.from(primaryMeeting.map((x) => x)),
    "speaker": List<dynamic>.from(speaker.map((x) => speakerValues.reverse[x])),
    "collaboration_meeting": collaborationMeetingValues.reverse[collaborationMeeting],
    "invited": invited,
    "confirmed_invitees": confirmedInvitees,
    "hcpOrRep": List<dynamic>.from(hcpOrRep.map((x) => x)),
    "participantStatus": participantStatus.toJson(),
  };
}

class Brand {
  BrandName brandName;
  BrandImage brandImage;
  UploadPath uploadPath;
  OriginalBrandImage originalBrandImage;
  int activeStatus;
  int deleteStatus;
  int createdBy;
  int updatedBy;
  int id;
  DateTime createdAt;
  DateTime updatedAt;
  int company;

  Brand({
    this.brandName,
    this.brandImage,
    this.uploadPath,
    this.originalBrandImage,
    this.activeStatus,
    this.deleteStatus,
    this.createdBy,
    this.updatedBy,
    this.id,
    this.createdAt,
    this.updatedAt,
    this.company,
  });

  factory Brand.fromJson(Map<String, dynamic> json) => Brand(
    brandName: brandNameValues.map[json["brand_name"]],
    brandImage: brandImageValues.map[json["brand_image"]],
    uploadPath: uploadPathValues.map[json["upload_path"]],
    originalBrandImage: originalBrandImageValues.map[json["original_brand_image"]],
    activeStatus: json["active_status"],
    deleteStatus: json["delete_status"],
    createdBy: json["created_by"],
    updatedBy: json["updated_by"],
    id: json["id"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    company: json["company"],
  );

  Map<String, dynamic> toJson() => {
    "brand_name": brandNameValues.reverse[brandName],
    "brand_image": brandImageValues.reverse[brandImage],
    "upload_path": uploadPathValues.reverse[uploadPath],
    "original_brand_image": originalBrandImageValues.reverse[originalBrandImage],
    "active_status": activeStatus,
    "delete_status": deleteStatus,
    "created_by": createdBy,
    "updated_by": updatedBy,
    "id": id,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "company": company,
  };
}

enum BrandImage { AVEXIS_LOGO_PNG }

final brandImageValues = EnumValues({
  "/avexis-logo.png": BrandImage.AVEXIS_LOGO_PNG
});

enum BrandName { AVE_XIS }

final brandNameValues = EnumValues({
  "AveXis": BrandName.AVE_XIS
});

enum OriginalBrandImage { AVEXIS_LOGO_PNG }

final originalBrandImageValues = EnumValues({
  "avexis-logo.png": OriginalBrandImage.AVEXIS_LOGO_PNG
});

enum UploadPath { IMAGES_COMPANYLOGO }

final uploadPathValues = EnumValues({
  "/images/companylogo": UploadPath.IMAGES_COMPANYLOGO
});

enum CollaborationMeeting { NO, YES }

final collaborationMeetingValues = EnumValues({
  "No": CollaborationMeeting.NO,
  "Yes": CollaborationMeeting.YES
});

class EngagementType {
  String type;
  String description;
  int orderNo;
  int isStart;
  int minimumHcp;
  int prescriberHcp;
  int isCollaborator;
  int isCohost;
  int isDetail;
  int isContentNote;
  int isQuestion;
  int amount;
  int hcpAmount;
  int activeStatus;
  int deleteStatus;
  int id;
  DateTime createdAt;
  DateTime updatedAt;

  EngagementType({
    this.type,
    this.description,
    this.orderNo,
    this.isStart,
    this.minimumHcp,
    this.prescriberHcp,
    this.isCollaborator,
    this.isCohost,
    this.isDetail,
    this.isContentNote,
    this.isQuestion,
    this.amount,
    this.hcpAmount,
    this.activeStatus,
    this.deleteStatus,
    this.id,
    this.createdAt,
    this.updatedAt,
  });

  factory EngagementType.fromJson(Map<String, dynamic> json) => EngagementType(
    type: json["type"],
    description: json["description"],
    orderNo: json["order_no"],
    isStart: json["is_start"],
    minimumHcp: json["minimum_hcp"],
    prescriberHcp: json["prescriber_hcp"],
    isCollaborator: json["is_collaborator"],
    isCohost: json["is_cohost"],
    isDetail: json["is_detail"],
    isContentNote: json["is_content_note"],
    isQuestion: json["is_question"],
    amount: json["amount"],
    hcpAmount: json["hcp_amount"],
    activeStatus: json["active_status"],
    deleteStatus: json["delete_status"],
    id: json["id"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
  );

  Map<String, dynamic> toJson() => {
    "type": typeValues.reverse[type],
    "description": description,
    "order_no": orderNo,
    "is_start": isStart,
    "minimum_hcp": minimumHcp,
    "prescriber_hcp": prescriberHcp,
    "is_collaborator": isCollaborator,
    "is_cohost": isCohost,
    "is_detail": isDetail,
    "is_content_note": isContentNote,
    "is_question": isQuestion,
    "amount": amount,
    "hcp_amount": hcpAmount,
    "active_status": activeStatus,
    "delete_status": deleteStatus,
    "id": id,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
  };
}

enum Type { LIVE_IN_OFFICE, CLINICAL_CONVERSATIONS }

final typeValues = EnumValues({
  "Clinical Conversations": Type.CLINICAL_CONVERSATIONS,
  "Live In-Office": Type.LIVE_IN_OFFICE
});

class Location {
  String locationName;
  String locationAddress;
  String locationCity;
  String locationState;
  String locationZip;
  EncryptedTokenReset locationPhone;
  int locationAmount;
  int id;
  AtedAt createdAt;
  AtedAt updatedAt;
  int engagementType;

  Location({
    this.locationName,
    this.locationAddress,
    this.locationCity,
    this.locationState,
    this.locationZip,
    this.locationPhone,
    this.locationAmount,
    this.id,
    this.createdAt,
    this.updatedAt,
    this.engagementType,
  });

  factory Location.fromJson(Map<String, dynamic> json) => Location(
    locationName: json["location_name"],
    locationAddress: json["location_address"],
    locationCity: json["location_city"],
    locationState: json["location_state"],
    locationZip: json["location_zip"],
    locationPhone: encryptedTokenResetValues.map[json["location_phone"]],
    locationAmount: json["location_amount"],
    id: json["id"],
    createdAt: atedAtValues.map[json["createdAt"]],
    updatedAt: atedAtValues.map[json["updatedAt"]],
    engagementType: json["engagementType"],
  );

  Map<String, dynamic> toJson() => {
    "location_name": locationName,
    "location_address": locationAddress,
    "location_city": locationCity,
    "location_state": locationState,
    "location_zip": locationZip,
    "location_phone": encryptedTokenResetValues.reverse[locationPhone],
    "location_amount": locationAmount,
    "id": id,
    "createdAt": atedAtValues.reverse[createdAt],
    "updatedAt": atedAtValues.reverse[updatedAt],
    "engagementType": engagementType,
  };
}

enum AtedAt { THE_00000000000000 }

final atedAtValues = EnumValues({
  "0000-00-00 00:00:00": AtedAt.THE_00000000000000
});

enum EncryptedTokenReset { NULL, EMPTY, FDFLGVDSCITPE }

final encryptedTokenResetValues = EnumValues({
  "": EncryptedTokenReset.EMPTY,
  "FDFLGVDSCITPE": EncryptedTokenReset.FDFLGVDSCITPE,
  "Null": EncryptedTokenReset.NULL
});

class ParticipantStatus {
  int participant;
  int userType;
  int status;
  int meeting;
  int waitingStatus;
  int pauseStatus;
  int leaveStatus;
  dynamic sessionId;
  dynamic token;
  int closeOutBy;
  int attendeesCloseOutMethod;
  int signInStatus;
  dynamic noShowComments;
  String signInImage;
  String signInImagePath;
  int optOutMeal;
  dynamic optLicState;
  int ffs;
  String sessionMins;
  int id;
  DateTime createdAt;
  DateTime updatedAt;

  ParticipantStatus({
    this.participant,
    this.userType,
    this.status,
    this.meeting,
    this.waitingStatus,
    this.pauseStatus,
    this.leaveStatus,
    this.sessionId,
    this.token,
    this.closeOutBy,
    this.attendeesCloseOutMethod,
    this.signInStatus,
    this.noShowComments,
    this.signInImage,
    this.signInImagePath,
    this.optOutMeal,
    this.optLicState,
    this.ffs,
    this.sessionMins,
    this.id,
    this.createdAt,
    this.updatedAt,
  });

  factory ParticipantStatus.fromJson(Map<String, dynamic> json) => ParticipantStatus(
    participant: json["participant"],
    userType: json["userType"],
    status: json["status"],
    meeting: json["meeting"],
    waitingStatus: json["waiting_status"],
    pauseStatus: json["pause_status"],
    leaveStatus: json["leave_status"],
    sessionId: json["sessionId"],
    token: json["token"],
    closeOutBy: json["closeOutBy"] == null ? null : json["closeOutBy"],
    attendeesCloseOutMethod: json["attendees_close_out_method"],
    signInStatus: json["sign_in_status"],
    noShowComments: json["no_show_comments"],
    signInImage: json["sign_in_image"] == null ? null : json["sign_in_image"],
    signInImagePath: json["sign_in_image_path"] == null ? null : json["sign_in_image_path"],
    optOutMeal: json["opt_out_meal"],
    optLicState: json["opt_lic_state"],
    ffs: json["ffs"],
    sessionMins: json["session_mins"],
    id: json["id"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
  );

  Map<String, dynamic> toJson() => {
    "participant": participant,
    "userType": userType,
    "status": status,
    "meeting": meeting,
    "waiting_status": waitingStatus,
    "pause_status": pauseStatus,
    "leave_status": leaveStatus,
    "sessionId": sessionId,
    "token": token,
    "closeOutBy": closeOutBy == null ? null : closeOutBy,
    "attendees_close_out_method": attendeesCloseOutMethod,
    "sign_in_status": signInStatus,
    "no_show_comments": noShowComments,
    "sign_in_image": signInImage == null ? null : signInImage,
    "sign_in_image_path": signInImagePath == null ? null : signInImagePath,
    "opt_out_meal": optOutMeal,
    "opt_lic_state": optLicState,
    "ffs": ffs,
    "session_mins": sessionMins,
    "id": id,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
  };
}

class ProductClass {
  String productName;
  ProductEnum product;
  String productDesc;
  String productIsi;
  int activeStatus;
  int deleteStatus;
  int createdBy;
  int updatedBy;
  int id;
  DateTime createdAt;
  DateTime updatedAt;
  int brand;
  int company;

  ProductClass({
    this.productName,
    this.product,
    this.productDesc,
    this.productIsi,
    this.activeStatus,
    this.deleteStatus,
    this.createdBy,
    this.updatedBy,
    this.id,
    this.createdAt,
    this.updatedAt,
    this.brand,
    this.company,
  });

  factory ProductClass.fromJson(Map<String, dynamic> json) => ProductClass(
    productName: json["product_name"],
    product: productEnumValues.map[json["product"]],
    productDesc: json["product_desc"],
    productIsi: json["product_isi"],
    activeStatus: json["active_status"],
    deleteStatus: json["delete_status"],
    createdBy: json["created_by"],
    updatedBy: json["updated_by"],
    id: json["id"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    brand: json["brand"],
    company: json["company"],
  );

  Map<String, dynamic> toJson() => {
    "product_name": productName,
    "product": productEnumValues.reverse[product],
    "product_desc": productDesc,
    "product_isi": productIsi,
    "active_status": activeStatus,
    "delete_status": deleteStatus,
    "created_by": createdBy,
    "updated_by": updatedBy,
    "id": id,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "brand": brand,
    "company": company,
  };
}

enum ProductEnum { ZOLGENSMA }

final productEnumValues = EnumValues({
  "ZOLGENSMA": ProductEnum.ZOLGENSMA
});

enum Speaker { SHADE_MOODY }

final speakerValues = EnumValues({
  "Shade Moody": Speaker.SHADE_MOODY
});

class User {
  String firstName;
  String lastName;
  String midName;
  String address2;
  String affiliation;
  String govtEmployee;
  String licensedState;
  String fullName;
  String preferredName;
  String email;
  String homeNumber;
  String telephone;
  dynamic preferredTelephone;
  String degree;
  dynamic speciality;
  dynamic institution;
  String address;
  String city;
  String state;
  String zip;
  int prescriber;
  Area area;
  Region region;
  String district;
  String territory;
  dynamic yrsOfExperience;
  String biography;
  String biographyPath;
  String cv;
  String cvPath;
  Photo photo;
  PhotoPath photoPath;
  dynamic publication;
  dynamic institutionAddress;
  dynamic institutionAddress1;
  dynamic institutionState;
  dynamic institutionZip;
  dynamic institutionRestriction;
  dynamic primaryEmail;
  dynamic secondryEmail;
  dynamic stateOfLicense;
  dynamic npi;
  dynamic decile;
  Designation designation;
  dynamic companyName;
  String userType;
  int attendeeType;
  int noOfLiveprograms;
  int capAmount;
  int capMaxLimit;
  int speakerStatus;
  int activeStatus;
  int deleteStatus;
  String deviceToken;
  EncryptedTokenReset encryptedTokenReset;
  int termsCondition;
  int resetPasswordFlag;
  int subscribeToNewsletter;
  int introVideo;
  int createdBy;
  int updatedBy;
  int waitingStatus;
  dynamic webinarOnly;
  dynamic patientUser;
  dynamic stripeId;
  String employeeId;
  dynamic imedId;
  dynamic hireDate;
  dynamic storageFacilityName;
  dynamic storagePhone;
  dynamic storageAddress;
  dynamic storageUnit;
  dynamic storageCity;
  dynamic storageState;
  dynamic storageZip;
  dynamic prodBags;
  dynamic lastUpdated;
  dynamic salesForceNote;
  int tier;
  int loginFlag;
  int id;
  AtedAt createdAt;
  DateTime updatedAt;
  dynamic rep;
  int dm;
  dynamic topic;
  int company;
  int role;

  User({
    this.firstName,
    this.lastName,
    this.midName,
    this.address2,
    this.affiliation,
    this.govtEmployee,
    this.licensedState,
    this.fullName,
    this.preferredName,
    this.email,
    this.homeNumber,
    this.telephone,
    this.preferredTelephone,
    this.degree,
    this.speciality,
    this.institution,
    this.address,
    this.city,
    this.state,
    this.zip,
    this.prescriber,
    this.area,
    this.region,
    this.district,
    this.territory,
    this.yrsOfExperience,
    this.biography,
    this.biographyPath,
    this.cv,
    this.cvPath,
    this.photo,
    this.photoPath,
    this.publication,
    this.institutionAddress,
    this.institutionAddress1,
    this.institutionState,
    this.institutionZip,
    this.institutionRestriction,
    this.primaryEmail,
    this.secondryEmail,
    this.stateOfLicense,
    this.npi,
    this.decile,
    this.designation,
    this.companyName,
    this.userType,
    this.attendeeType,
    this.noOfLiveprograms,
    this.capAmount,
    this.capMaxLimit,
    this.speakerStatus,
    this.activeStatus,
    this.deleteStatus,
    this.deviceToken,
    this.encryptedTokenReset,
    this.termsCondition,
    this.resetPasswordFlag,
    this.subscribeToNewsletter,
    this.introVideo,
    this.createdBy,
    this.updatedBy,
    this.waitingStatus,
    this.webinarOnly,
    this.patientUser,
    this.stripeId,
    this.employeeId,
    this.imedId,
    this.hireDate,
    this.storageFacilityName,
    this.storagePhone,
    this.storageAddress,
    this.storageUnit,
    this.storageCity,
    this.storageState,
    this.storageZip,
    this.prodBags,
    this.lastUpdated,
    this.salesForceNote,
    this.tier,
    this.loginFlag,
    this.id,
    this.createdAt,
    this.updatedAt,
    this.rep,
    this.dm,
    this.topic,
    this.company,
    this.role,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
    firstName: json["firstName"],
    lastName: json["lastName"],
    midName: json["midName"],
    address2: json["address2"],
    affiliation: json["affiliation"],
    govtEmployee: json["govt_employee"],
    licensedState: json["licensed_state"],
    fullName: json["fullName"],
    preferredName: json["preferred_name"],
    email: json["email"],
    homeNumber: json["home_number"],
    telephone: json["telephone"],
    preferredTelephone: json["preferred_telephone"],
    degree: json["degree"],
    speciality: json["speciality"],
    institution: json["institution"],
    address: json["address"],
    city: json["city"],
    state: json["state"],
    zip: json["zip"],
    prescriber: json["prescriber"],
    area: areaValues.map[json["area"]],
    region: regionValues.map[json["region"]],
    district: json["district"],
    territory: json["territory"],
    yrsOfExperience: json["yrsOfExperience"],
    biography: json["biography"],
    biographyPath: json["biography_path"],
    cv: json["cv"],
    cvPath: json["cv_path"],
    photo: photoValues.map[json["photo"]],
    photoPath: photoPathValues.map[json["photo_path"]],
    publication: json["publication"],
    institutionAddress: json["institution_address"],
    institutionAddress1: json["institution_address1"],
    institutionState: json["institution_state"],
    institutionZip: json["institution_zip"],
    institutionRestriction: json["institution_restriction"],
    primaryEmail: json["primary_email"],
    secondryEmail: json["secondry_email"],
    stateOfLicense: json["state_of_license"],
    npi: json["npi"],
    decile: json["decile"],
    designation: designationValues.map[json["designation"]],
    companyName: json["companyName"],
    userType: json["userType"],
    attendeeType: json["attendeeType"],
    noOfLiveprograms: json["no_of_liveprograms"],
    capAmount: json["cap_amount"],
    capMaxLimit: json["cap_max_limit"],
    speakerStatus: json["speaker_status"],
    activeStatus: json["active_status"],
    deleteStatus: json["delete_status"],
    deviceToken: json["deviceToken"] == null ? null : json["deviceToken"],
    encryptedTokenReset: encryptedTokenResetValues.map[json["encrypted_token_reset"]],
    termsCondition: json["terms_condition"],
    resetPasswordFlag: json["reset_password_flag"],
    subscribeToNewsletter: json["subscribe_to_newsletter"],
    introVideo: json["intro_video"],
    createdBy: json["created_by"],
    updatedBy: json["updated_by"],
    waitingStatus: json["waiting_status"],
    webinarOnly: json["webinar_only"],
    patientUser: json["patient_user"],
    stripeId: json["stripeId"],
    employeeId: json["employee_id"],
    imedId: json["imed_id"],
    hireDate: json["hire_date"],
    storageFacilityName: json["storage_facility_name"],
    storagePhone: json["storage_phone"],
    storageAddress: json["storage_address"],
    storageUnit: json["storage_unit"],
    storageCity: json["storage_city"],
    storageState: json["storage_state"],
    storageZip: json["storage_zip"],
    prodBags: json["prod_bags"],
    lastUpdated: json["last_updated"],
    salesForceNote: json["sales_force_note"],
    tier: json["tier"],
    loginFlag: json["login_flag"],
    id: json["id"],
    createdAt: atedAtValues.map[json["createdAt"]],
    updatedAt: DateTime.parse(json["updatedAt"]),
    rep: json["rep"],
    dm: json["dm"],
    topic: json["topic"],
    company: json["company"],
    role: json["role"],
  );

  Map<String, dynamic> toJson() => {
    "firstName": firstName,
    "lastName": lastName,
    "midName": midName,
    "address2": address2,
    "affiliation": affiliation,
    "govt_employee": govtEmployee,
    "licensed_state": licensedState,
    "fullName": fullName,
    "preferred_name": preferredName,
    "email": email,
    "home_number": homeNumber,
    "telephone": telephone,
    "preferred_telephone": preferredTelephone,
    "degree": degree,
    "speciality": speciality,
    "institution": institution,
    "address": address,
    "city": city,
    "state": state,
    "zip": zip,
    "prescriber": prescriber,
    "area": areaValues.reverse[area],
    "region": regionValues.reverse[region],
    "district": district,
    "territory": territory,
    "yrsOfExperience": yrsOfExperience,
    "biography": biography,
    "biography_path": biographyPath,
    "cv": cv,
    "cv_path": cvPath,
    "photo": photoValues.reverse[photo],
    "photo_path": photoPathValues.reverse[photoPath],
    "publication": publication,
    "institution_address": institutionAddress,
    "institution_address1": institutionAddress1,
    "institution_state": institutionState,
    "institution_zip": institutionZip,
    "institution_restriction": institutionRestriction,
    "primary_email": primaryEmail,
    "secondry_email": secondryEmail,
    "state_of_license": stateOfLicense,
    "npi": npi,
    "decile": decile,
    "designation": designationValues.reverse[designation],
    "companyName": companyName,
    "userType": userType,
    "attendeeType": attendeeType,
    "no_of_liveprograms": noOfLiveprograms,
    "cap_amount": capAmount,
    "cap_max_limit": capMaxLimit,
    "speaker_status": speakerStatus,
    "active_status": activeStatus,
    "delete_status": deleteStatus,
    "deviceToken": deviceToken == null ? null : deviceToken,
    "encrypted_token_reset": encryptedTokenResetValues.reverse[encryptedTokenReset],
    "terms_condition": termsCondition,
    "reset_password_flag": resetPasswordFlag,
    "subscribe_to_newsletter": subscribeToNewsletter,
    "intro_video": introVideo,
    "created_by": createdBy,
    "updated_by": updatedBy,
    "waiting_status": waitingStatus,
    "webinar_only": webinarOnly,
    "patient_user": patientUser,
    "stripeId": stripeId,
    "employee_id": employeeId,
    "imed_id": imedId,
    "hire_date": hireDate,
    "storage_facility_name": storageFacilityName,
    "storage_phone": storagePhone,
    "storage_address": storageAddress,
    "storage_unit": storageUnit,
    "storage_city": storageCity,
    "storage_state": storageState,
    "storage_zip": storageZip,
    "prod_bags": prodBags,
    "last_updated": lastUpdated,
    "sales_force_note": salesForceNote,
    "tier": tier,
    "login_flag": loginFlag,
    "id": id,
    "createdAt": atedAtValues.reverse[createdAt],
    "updatedAt": updatedAt.toIso8601String(),
    "rep": rep,
    "dm": dm,
    "topic": topic,
    "company": company,
    "role": role,
  };
}

enum Area { K0200, K0100, K0400 }

final areaValues = EnumValues({
  "K0100": Area.K0100,
  "K0200": Area.K0200,
  "K0400": Area.K0400
});

enum Designation { RBD }

final designationValues = EnumValues({
  "RBD": Designation.RBD
});

enum Photo { EMPTY, THE_8_FE08_C0_F_7_FFA_4_E20_81_FA_4_CAB81239_C05_JPG }

final photoValues = EnumValues({
  "": Photo.EMPTY,
  "/8fe08c0f-7ffa-4e20-81fa-4cab81239c05.jpg": Photo.THE_8_FE08_C0_F_7_FFA_4_E20_81_FA_4_CAB81239_C05_JPG
});

enum PhotoPath { EMPTY, IMAGES_PROFILEIMAGE }

final photoPathValues = EnumValues({
  "": PhotoPath.EMPTY,
  "/images/profileimage": PhotoPath.IMAGES_PROFILEIMAGE
});

enum Region { SOUTH, NORTHEAST, WEST }

final regionValues = EnumValues({
  "NORTHEAST": Region.NORTHEAST,
  "SOUTH": Region.SOUTH,
  "WEST": Region.WEST
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
