import 'package:flutter/material.dart';

class EmptyItemContainer extends StatelessWidget {
  final bool isTablet;
  final String message;
  final Color color;
  final FontWeight fontWeight;

  @override
  Widget build(BuildContext context) {
    return Text(
      message ?? '',
      style: TextStyle(
        fontSize: isTablet ? 18.0 : 16.0,
        color: color ?? Colors.black,
        fontWeight: fontWeight ?? FontWeight.w500,
      ),
    );
  }

  EmptyItemContainer({
    this.isTablet = false,
    this.message,
    this.color,
    this.fontWeight,
  });
}
