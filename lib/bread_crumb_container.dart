import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

import 'CommonUtils.dart';
import 'WebViewStream.dart';

class BreadCrumbContainer extends StatelessWidget {
  const BreadCrumbContainer({
    Key key,
    @required this.isTabletDevice,
    this.filterOnTap,
    this.filtered = false,
  }) : super(key: key);

  final bool isTabletDevice;
  final bool filtered;
  final Function filterOnTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).buttonColor,
      child: Padding(
        padding:
            EdgeInsets.only(right: 15.0, left: 15.0, top: 8.0, bottom: 8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              'Avexis' ?? 'Brand',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: isTabletDevice ? 16.0 : 14.0,
                  fontWeight: FontWeight.w600),
            ),
            Icon(
              Icons.arrow_forward_ios,
              size: 14,
              color: Colors.white,
            ),
            Flexible(
              child: Html(
                data: 'Test Product' ?? 'Products',
                useRichText: false,
                //Optional parameters:
                defaultTextStyle: TextStyle(
                    color: Colors.white,
                    fontSize: isTabletDevice ? 16.0 : 14.0,
                    fontWeight: FontWeight.w600),
                linkStyle: TextStyle(
                  color: Colors.red,
                  fontSize: isTabletDevice ? 16.0 : 14.0,
                  fontWeight: FontWeight.w400,
                ),
                onLinkTap: (url) {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              WebViewStream(title: '', selectedUrl: url)));
                },
              ),
            ),
            GestureDetector(
                onTap: filterOnTap,
                child: Container(
//                    margin: EdgeInsets.only(right: 0, top: 5, bottom: 5),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(3),
                    ),
                    color: Colors.white,
                  ),
                  child: Padding(
                    padding: EdgeInsets.only(
                      left: 8,
                      right: 8,
                    ),
                    child: Image.asset(
                      filtered
                          ? CommonUtils.assetsImage('Filter')
                          : CommonUtils.assetsImage('Filter'),
                      width: 23,
                      height: 30,
                    ),
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
