import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:rdcloseoutsession/network/Check.dart';
import 'package:rdcloseoutsession/network/custom_treeview.dart';
import 'package:rdcloseoutsession/network/geolist.dart';

class Header {
  final String title;
  final List<Document> expandchild;
  bool value;

  Header({this.title, this.expandchild, this.value});
}

class Document {
  final String name;
  final bool isFile;

  final List<Document> childData;
  final String subtitle;

  Document({
    this.subtitle,
    @required this.name,
    this.isFile = false,
    this.childData = const <Document>[],
  });
}

class FreshState extends StatefulWidget {
  FreshState({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _FreshState createState() => _FreshState();
}

class _FreshState extends State<FreshState> {
  GeoModel _geoModel = null;
  PageStorageKey _key;
  List<int> initArray = new List();
  List<Header> headerlist = [
    Header(
      title: "Area",
      value: false,
    ),
    Header(title: "Region", value: false),
  ];

  List<Document> _documentList = [];

  bool value = false;
  bool isSelected = false;
  bool _value = false;

  void _arrowIcon() {
    setState(() {
      _value = !_value;
      print(_value);
    });
  }

  @override
  void initState() {
    super.initState();
    getTopicInfo();
  }

  void getTopicInfo() async {
    String data =
        await DefaultAssetBundle.of(context).loadString("assets/geoList.json");
    setState(() {
      _geoModel = GeoModel.fromJson(data);
    }); //    debugPrint('Profile Information --> ${userProfile.statusCode}', wrapWidth: 1024);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        title: Text(
          'Collaboration',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w500,
            fontSize: 16,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios, color: Color(0xFF399895)),
          iconSize: 20,
          onPressed: () => Navigator.push(context,
              MaterialPageRoute(builder: (context) => ExpansionTileSample())),
        ),
//        bottom: PreferredSize(
//          child: Container(
//            color: Color(0xFF399895),
//            height: 1.0,
//          ),
//        ),
      ),
      body: Container(
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.end,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListView.builder(
              shrinkWrap: true,
              physics: AlwaysScrollableScrollPhysics(),
              scrollDirection: Axis.vertical,
              itemCount: headerlist == null ? 0 : headerlist.length,
              itemBuilder: (context, index) {
                return ListTile(
                  dense: true,
                  onTap: () {
                    buildExpanded();
                    setState(() {
//                        _value =!_value;
                      headerlist[index].value = !headerlist[index].value;
                      print('test value > $value');
                    });
                  },
                  title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Text(
                              headerlist[index].title,
                              style: TextStyle(
                                fontSize: 15,
                              ),
                            ),
                          ],
                        ),
                        Container(
                            child: headerlist[index].value
                                ? new Icon(Icons.keyboard_arrow_down)
                                : Icon(Icons.keyboard_arrow_right)),
                      ]),
                );
              },
            ),
//            Expanded(
//              child: Visibility(
//                visible: true,
//                child: TreeView(
//                  parentList: _getParentList(),
//                ),
//              ),
//            ),
//            buildExpanded(),
          ],
        ),
      ),
    );
  }

  _buildExpandableContent() {
    List<Parent> parentList = [];

    if (_geoModel != null) {
      _geoModel.result.geo.forEach((geo) {
        Parent parent = _getParent(document: getGeoLocationList(geo));
        parentList.add(parent);
      });
    }

    return parentList;
  }

  Expanded buildExpanded() {
    print('enter the expand view');
    return Expanded(
      child: Visibility(
        visible: true,
        child: TreeView(
          parentList: _getParentList(),
        ),
      ),
    );
  }

  List<Parent> _getParentList() {
    List<Parent> parentList = [];

    if (_geoModel != null) {
      _geoModel.result.geo.forEach((geo) {
        Parent parent = _getParent(document: getGeoLocationList(geo));
        parentList.add(parent);
      });
    }

    return parentList;
  }

  Parent _getParent({@required Document document}) {
    ChildList childList =
        document.isFile ? null : _getChildList(document: document);

    return Parent(
      parent: _getDocumentWidget(document: document),
      childList: childList,
    );
  }

  ChildList _getChildList({@required Document document}) {
    List<Widget> widgetList = [];

    List<Document> childDocuments = document.childData;
    childDocuments.forEach((childDocument) {
      widgetList.add(Container(
//        color: Color(0xFFe3e6ef),
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.only(
            left: 16.0,
          ),
          child: _getParent(document: childDocument),
        ),
      ));
    });

    return ChildList(children: widgetList);
  }

  Widget _getDocumentWidget({@required Document document}) => document.isFile
      ? _getFileWidget(document: document)
      : _getDirectoryWidget(document: document);

  DirectoryWidget _getDirectoryWidget({@required Document document}) =>
      DirectoryWidget(
        directoryName: document.name,
        name: document.subtitle,
        ontap: () {
          setState(() {
            isSelected = false;
            print(
              document.name,
            );
          });
        },
      );

  FileWidget _getFileWidget({@required Document document}) => FileWidget(
        fileName: document.name,
      );
}

Document getGeoLocationList(Geo geo) {
  return Document(name: '${geo.fieldName}', subtitle: "", childData: [
    for (var geo1 in geo.geo1)
      Document(name: '${geo1.fieldName}', subtitle: "", childData: [
        for (var geo2 in geo1.geo2)
          Document(
            name: '${geo2.fieldName}',
            subtitle: "",
            childData: [
              for (var geo3 in geo2.geo3)
                Document(
                  name: '${geo3.fieldName}',
                  subtitle: "",
                  childData: [
                    for (var rep in geo3.repUsers)
                      Document(
                        name: '${rep.firstName},${rep.lastName}',
                        isFile: false,
                        subtitle: '${rep.city},${rep.state}',
                      ),
                  ],
                ),
            ],
          ),
      ])
  ]);
}

class DirectoryWidget extends StatelessWidget {
  final String directoryName;
  final String name;
  final DateTime lastModified;
  final VoidCallback onPressedNext;
  final bool isSelected;
  final Function ontap;
  final bool monVal;
  DirectoryWidget({
    @required this.directoryName,
    @required this.lastModified,
    this.onPressedNext,
    this.isSelected = false,
    this.name,
    this.ontap,
    this.monVal = false,
  });

  @override
  Widget build(BuildContext context) {
    Widget titleWidget = Text(directoryName);
    Widget subtitleWidget = Visibility(visible: true, child: Text(name));

//    Container expandButton = Container(
//      child: checkbox(),
//    );

    return ListTile(
      title: titleWidget,
      subtitle: subtitleWidget,
//      trailing: expandButton,
    );
  }
}

class FileWidget extends StatelessWidget {
  final String fileName;
  bool monVal = false;
  final String checkbox;

  FileWidget({
    @required this.fileName,
    this.checkbox,
  });
  @override
  Widget build(BuildContext context) {
    Widget fileNameWidget = Text(this.fileName);

    return ListTile(
      title: fileNameWidget,
    );
  }
}
