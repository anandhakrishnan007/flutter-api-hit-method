import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;

import 'CustomException.dart';

class ApiProvider {
  http.Response response;
  String token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MjMsInVzZXJUeXBlIjoiMSIsImNvbXBhbnkiOjMsInJvbGUiOjEsImlhdCI6MTU4NjM1MjY1Mn0.CNCq5YDBI_j-QjKI01NRw23CHTH6W3xMJWw08cbe7cs';

  Future<dynamic> callNetworkRequest(
      String url, Map<String, String> params, int methodType) async {
    var responseJson;

    try {
      if (methodType == 0) {
        response = await http.get(url,
            headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
        responseJson = _response(response);
      } else if (methodType == 1) {
        response = await http.post(url,
            body: params,
            headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
        responseJson = _response(response);
      } else if (methodType == 2) {
        response = await http.put(url,
            body: params,
            headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
        responseJson = _response(response);
      } else {
        response = await http.delete(url);
        responseJson = _response(response);
      }
    } on SocketException {
//      throw FetchDataException('No Internet connection');
    }

//    try {
//      print('url >> $url');
//      final response = awairesponseJsont http.get(_baseUrl + url);
//      responseJson = _response(response);
//    } on SocketException {
//      throw FetchDataException('No Internet connection');
//    }
    return responseJson;
  }

//=================================================================

//  final String _baseUrl = EndPointConstants.BASE_URL_AVEXIS_CORE;
//  String token = SharedPrefUtils.getString(StringConstants.userToken);
//
//  Future<dynamic> get(String url, Map<String, String> params) async {
//    var responseJson;
//    try {
//      print('url >> $_baseUrl+$url');
//      final response = await http.post(_baseUrl + url,
//          body: params,
//          headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
//      responseJson = _response(response);
//      print('url json >> $responseJson');
//    } on SocketException {
//      throw FetchDataException('No Internet connection');
//    }
//    return responseJson;
//  }

  dynamic _response(http.Response response) {
    print("print token > $token");
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body);
//        print("print json > $responseJson");
        print(" print success " + response.body.toString());
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:

      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:

      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }
}
