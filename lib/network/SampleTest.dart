import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:rdcloseoutsession/custom_app_bar.dart';
import 'package:rdcloseoutsession/custom_app_theme.dart';
import 'package:rdcloseoutsession/custom_dimen.dart';
import 'package:rdcloseoutsession/fallback_avatar.dart';
import 'package:rdcloseoutsession/network/custom_collaborator_expansion_tile.dart';
import 'package:rdcloseoutsession/network/custom_treeview.dart';
import 'package:rdcloseoutsession/network/geolist.dart';

class Header {
  final String title;
  final List<Document> expandChild;
  bool value;

  Header({this.title, this.expandChild, this.value});
}

class Document {
  final String name;
  final bool isFile;
  bool isChecked;
  final List<Document> childData;
  final String subtitle;
  final String fieldId;
  final String lastName;
  final String firstName;
  final bool innerSection;
  double docindex;
  final ValueChanged<Object> onItemChanged;

  Document({
    this.subtitle,
    @required this.name,
    this.isFile = false,
    this.isChecked = false,
    this.onItemChanged,
    this.fieldId,
    this.lastName,
    this.firstName,
    this.innerSection,
    this.docindex,
    this.childData = const <Document>[],
  });
}

class CollaboratorSelection extends StatefulWidget {
  CollaboratorSelection({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _CollaboratorSelectionState createState() => _CollaboratorSelectionState();
}

class _CollaboratorSelectionState extends State<CollaboratorSelection> {
  ScrollController _controller = new ScrollController();
  double marg = 1.0;
  GeoModel _geoModel = null;
  GeoModel _tempGeoModel = null;
  PageStorageKey _key;
  List<int> initArray = new List();
  bool showColor = false;
  List<Header> headerlist = [
    Header(
      title: "Area",
      value: false,
    ),
    Header(title: "Region", value: false),
  ];

  List<Document> _documentList = [];

  bool value = false;
  bool isSelected = false;
  bool _value = false;

  void _arrowIcon() {
    setState(() {
      _value = !_value;
      print(_value);
    });
  }

  @override
  void initState() {
    super.initState();
    getTopicInfo();
  }

  void getTopicInfo() async {
    String data =
        await DefaultAssetBundle.of(context).loadString("assets/geoList.json");
    setState(() {
      _geoModel = GeoModel.fromJson(data);
      _tempGeoModel = GeoModel.fromJson(data);
    }); //    debugPrint('Profile Information --> ${userProfile.statusCode}', wrapWidth: 1024);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CustomAppBar(
        title: 'Collaborator',
      ),
      body: Container(
//        height: 800,
        color: Colors.white,
        child: SingleChildScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          controller: _controller,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                scrollDirection: Axis.vertical,
                controller: _controller,
                itemCount: headerlist == null ? 0 : headerlist.length,
                itemBuilder: (context, index) {
                  return Column(
                    children: [
                      Theme(
                        data: Theme.of(context)
                            .copyWith(accentColor: Colors.black),
                        child: new CollabotatorExpansionTile(
                          key: Key(index.toString()),
                          title: new Text(
                            "${headerlist[index].title}",
                            style: TextStyle(
                                fontWeight: FontWeight.w600, fontSize: 16.0),
                          ),
                          backgroundColor: Colors.white,
                          children: <Widget>[
                            new Column(children: _buildExpandableContent()),
                          ],
                        ),
                      ),
                      Divider(
                        height: 0.0,
                      )
                    ],
                  );
                },
              ),
//            Expanded(
//              child: Visibility(
//                visible: true,
//                child: TreeView(
//                  parentList: _getParentList(),
//                ),
//              ),
//            ),
//            buildExpanded(),
            ],
          ),
        ),
      ),
    );
  }

  _buildExpandableContent() {
    List<Parent> parentList = [];

    if (_geoModel != null) {
      _geoModel.result.geo.forEach((geo) {
        Parent parent = _getParent(document: getGeoLocationList(geo));
        parentList.add(parent);
      });
    }

    return parentList;
  }

  Expanded buildExpanded() {
    return Expanded(
      child: Visibility(
        visible: true,
        child: TreeView(
          parentList: _getParentList(),
        ),
      ),
    );
  }

  List<Parent> _getParentList() {
    marg = 5.0;
    List<Parent> parentList = [];

    if (_geoModel != null) {
      _geoModel.result.geo.forEach((geo) {
        Parent parent = _getParent(document: getGeoLocationList(geo));
        parentList.add(parent);
      });
    }

    return parentList;
  }

  Parent _getParent({@required Document document}) {
    marg = marg + 8.9;
    if (document.innerSection) {
      marg = 1.0;
    }
    print(" margin values $marg");

    Document documentdata = document;
    Document tempdocumentdata = document;
    tempdocumentdata.docindex = marg;
    ChildList childList = tempdocumentdata.isFile
        ? null
        : _getChildList(document: tempdocumentdata);

    return Parent(
      parent: _getLocationWidget(document: tempdocumentdata),
      childList: childList,
      documentdata: tempdocumentdata,
      isShowLeading: tempdocumentdata.subtitle.isEmpty ? true : false,
      showColor: tempdocumentdata.innerSection,
    );
  }

  ChildList _getChildList({@required Document document}) {
    List<Widget> widgetList = [];
    List<Document> childDocuments = document.childData;

    childDocuments.forEach((childDocument) {
      marg = marg + 1.9;
      Document tempdocumentdata = childDocument;

      widgetList.add(Container(
          color: tempdocumentdata.innerSection
              ? Colors.white
              : CustomAppTheme.bgGreyColor,
          child: tempdocumentdata.innerSection
              ? Container(
                  color: Colors.white,
                  margin: EdgeInsets.only(
                    left: 16.0,
                  ),
                  child: _getParent(document: tempdocumentdata),
                )
              : Container(
                  color: Colors.white,
                  margin: EdgeInsets.only(
                    left: 0.0,
                  ),
                  child: _getParent(document: tempdocumentdata),
                )));
    });

    return ChildList(children: widgetList);
  }

  Widget _getLocationWidget({@required Document document}) =>
      document.subtitle.isEmpty
          ? _getDirectoryWidget(document: document)
          : _getRepWidget(document: document);

  LocationWidget _getDirectoryWidget({@required Document document}) =>
      LocationWidget(
        directoryName: "${document.name}",
        name: "${document.name}",
        ontap: () {
          setState(() {
            isSelected = false;
            print(
              {' doc name  $document.name'},
            );
          });
        },
      );

  RepWidget _getRepWidget({@required Document document}) => RepWidget(
        fileName: document.name,
        subTitle: document.subtitle,
        firstName: document.firstName,
        lastName: document.lastName,
      );

  void firstGeoLocationUpdate(Document document) {
    setState(() {
      for (var geo in _geoModel.result.geo) {
        if (geo.isslected) {
          geo.isslected = false;
        } else {
          geo.isslected = true;
        }
        for (var geo1 in geo.geo1) {
          geo1.isslected = geo.isslected;
          for (var geo2 in geo1.geo2) {
            geo2.isslected = geo1.isslected;
            for (var geo3 in geo2.geo3) {
              geo3.isslected = geo2.isslected;
              for (var repuser in geo3.repUsers) {
                repuser.isselected = geo3.isslected;
              }
            }
          }
        }
      }
    });
  }

  void secondGeoLocationUpdate(Document document) {
    setState(() {
      for (var geo in _geoModel.result.geo) {
        for (var geo1 in geo.geo1) {
          for (var geo2 in geo1.geo2) {
            if (geo2.fieldId == document.fieldId) {
              if (geo2.isslected) {
                geo2.isslected = false;
              } else {
                geo2.isslected = true;
              }

              for (var geo3 in geo2.geo3) {
                geo3.isslected = geo2.isslected;
                for (var repuser in geo3.repUsers) {
                  repuser.isselected = geo3.isslected;
                }
              }
            }
          }
        }
      }
    });
  }

  void thirdGeoLocationUpdate(Document document) {
    setState(() {
      for (var geo in _geoModel.result.geo) {
        for (var geo1 in geo.geo1) {
          for (var geo2 in geo1.geo2) {
            for (var geo3 in geo2.geo3) {
              if (geo3.fieldId == document.fieldId) {
                if (geo3.isslected) {
                  geo3.isslected = false;
                } else {
                  geo3.isslected = true;
                }

                for (var repuser in geo3.repUsers) {
                  repuser.isselected = geo3.isslected;
                }
              }
            }
          }
        }
      }
    });
  }

  void repUserUpdate(Document document) {
    setState(() {
      for (var geo in _geoModel.result.geo) {
        for (var geo1 in geo.geo1) {
          for (var geo2 in geo1.geo2) {
            for (var geo3 in geo2.geo3) {
              for (var repuser in geo3.repUsers) {
                if (document.fieldId == "${repuser.id}") {
                  if (repuser.isselected) {
                    repuser.isselected = false;
                  } else {
                    repuser.isselected = true;
                  }
                }
              }
            }
          }
        }
      }
    });
  }

  Document getGeoLocationList(Geo geo) {
    return Document(
        name: '${geo.fieldName}',
        isFile: false,
        subtitle: "",
        innerSection: false,
        onItemChanged: (value) {
          print("parentemit edmit ${value}");
          firstGeoLocationUpdate(value);
        },
        isChecked: geo.isslected,
        fieldId: geo.fieldId,
        childData: [
          for (var geo1 in geo.geo1)
            Document(
                name: '${geo1.fieldName}',
                subtitle: "",
                isFile: false,
                innerSection: false,
                onItemChanged: (value) {
                  print("geo1 edmit ${value}");
                  firstGeoLocationUpdate(value);
                },
                isChecked: geo1.isslected,
                fieldId: geo1.fieldId,
                childData: [
                  for (var geo2 in geo1.geo2)
                    Document(
                      name: '${geo2.fieldName}',
                      isFile: false,
                      innerSection: false,
                      onItemChanged: (value) {
                        print("geo2 edmit ${value}");
                        secondGeoLocationUpdate(value);
                      },
                      subtitle: "",
                      isChecked: geo2.isslected,
                      fieldId: geo2.fieldId,
                      childData: [
                        for (var geo3 in geo2.geo3)
                          Document(
                            name: '${geo3.fieldName}',
                            isFile: false,
                            innerSection: false,
                            onItemChanged: (value) {
                              print("geo3 edmit ${value}");
                              thirdGeoLocationUpdate(value);
                            },
                            subtitle: "",
                            isChecked: geo3.isslected,
                            fieldId: geo3.fieldId,
                            childData: [
                              for (var rep in geo3.repUsers)
                                Document(
                                    name: '${rep.firstName},${rep.lastName}',
                                    isFile: false,
                                    onItemChanged: (value) {
                                      print(
                                          "Rep edmit ${(value as Document).name}");
                                      repUserUpdate(value);
                                    },
                                    subtitle: '${rep.city},${rep.state}',
                                    isChecked: rep.isselected,
                                    fieldId: "${rep.id}",
                                    firstName: "${rep.firstName}",
                                    lastName: "${rep.lastName}",
                                    innerSection: true),
                            ],
                          ),
                      ],
                    ),
                ])
        ]);
  }
}

class LocationWidget extends StatelessWidget {
  final String directoryName;
  final String name;
  final DateTime lastModified;
  final VoidCallback onPressedNext;
  final bool isSelected;
  final Function ontap;
  final bool monVal;

  LocationWidget({
    @required this.directoryName,
    @required this.lastModified,
    this.onPressedNext,
    this.isSelected = false,
    this.name,
    this.ontap,
    this.monVal = false,
  });

  @override
  Widget build(BuildContext context) {
    Widget titleWidget = Text(
      directoryName,
      style: TextStyle(fontSize: 15.0),
    );
    Widget subtitleWidget = Visibility(visible: false, child: Text(name));

//    Container expandButton = Container(
//      child: checkbox(),
//    );

//    return ListTile(
//      title: titleWidget,
//      dense: true,
//      contentPadding: EdgeInsets.symmetric(vertical: 0.0,horizontal: 0.0),
////      subtitle: subtitleWidget,
////      trailing: expandButton,
//    );

    return Container(
      child: titleWidget,
    );
  }
}

class RepWidget extends StatelessWidget {
  final String fileName;
  final String subTitle;
  bool monVal = false;
  final String checkbox;
  final String firstName;
  final String lastName;

  RepWidget({
    @required this.fileName,
    this.subTitle,
    this.checkbox,
    this.firstName,
    this.lastName,
  });

  @override
  Widget build(BuildContext context) {
    Widget fileNameWidget = Text(this.fileName);
    return Container(
      color: Colors.white,
      child: ListTile(
        onTap: () {
          monVal = !monVal;
        },
        dense: true,
        contentPadding: EdgeInsets.all(0.0),
        title: Row(
          children: <Widget>[
            Visibility(
              visible: true,
              child: Row(
                children: <Widget>[
                  FallBackAvatar(
                    image: '',
                    initials: initialInfo(firstName, lastName) ?? '',
                    heightWeight: 35,
                    textStyle: CustomAppTheme.title.copyWith(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        fontSize: 13.0),
                    circleBackground: CustomAppTheme.bgGreyColor,
                    radius: 35 / 2,
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                ],
              ),
            ),

            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(
                        this.fileName,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: CustomAppTheme.textTheme.headline.copyWith(
                          fontWeight: FontWeight.w400,
                          fontSize: CustomDimension.textSizeVerySmall,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                  Visibility(
                    visible: this.subTitle != null && this.subTitle != '',
                    child: Padding(
                      padding: const EdgeInsets.only(top: 2.0),
                      child: Text(
                        this.subTitle ?? '',
                        style: TextStyle(
                            fontSize: CustomDimension.textSizeExtraVerySmall,
                            color: Colors.grey[700],
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                  )
                ],
              ),
            )
//          Expanded(
//            child: Column(
//              mainAxisAlignment: MainAxisAlignment.start,
//              crossAxisAlignment: CrossAxisAlignment.start,
//              children: <Widget>[
//                Row(
//                  children: <Widget>[
//                    Flexible(
//                      child: Text(
//                        this.fileName,
//                        style: TextStyle(fontWeight: FontWeight.w600),
//                      ),
//                    ),
//                  ],
//                ),
//                Container(
//                  margin: const EdgeInsets.only(top: 5.0),
//                  child: Text(
//                    this.subTitle,
//                    style: TextStyle(fontWeight: FontWeight.w600),
//                  ),
//                ),
//              ],
//            ),
//          ),
          ],
        ),
      ),
    );
  }

  String initialInfo(String fName, String lName) {
    String rtnInitial = '';

    String firstletter = fName[0];

    if (lName != null && lName != "") {
      String lastletter = lName[0];
      rtnInitial = firstletter + lastletter;
    } else {
      rtnInitial = firstletter;
    }
    print('initialll >> $rtnInitial');

    return rtnInitial.toString();
  }
}
