import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:rdcloseoutsession/Response.dart';
import 'package:rdcloseoutsession/list_engagement.dart';
import 'package:rdcloseoutsession/network/ListEngagementRepository.dart';

class ListEngagementBloc {
  ListEngagementRepository _listRepository;
  StreamController _engagementListController;

  StreamSink<Response<ListEngagement>> get engagementListSink =>
      _engagementListController.sink;

  Stream<Response<ListEngagement>> get engagementListStream =>
      _engagementListController.stream;
  ValueChanged<ListEngagement> onupdatelistiner;
  ListEngagementBloc(String startTime, String endTime, int page, String limit,
      String dashBoard, String filterrep, ValueChanged<ListEngagement> onupdatelistiner) {
    _engagementListController = StreamController<Response<ListEngagement>>();
    _listRepository = ListEngagementRepository();
    fetchListEngagementData(
        "Today", startTime, endTime, page, limit, dashBoard, filterrep,onupdatelistiner);
  }

  fetchListEngagementData(String enggType, String startTime, String endTime,
      int page, String limit, String dashBoard, String filterrep, ValueChanged<ListEngagement> onupdatelistiner) async {
    print("enggType >> $enggType");
    engagementListSink.add(Response.loading('load value'));
    try {
      ListEngagement chuckCats = await _listRepository.fetchChuckCategoryData(
          enggType, startTime, endTime, limit, page, filterrep, dashBoard,onupdatelistiner);
      engagementListSink.add(Response.completed(chuckCats));
    } catch (e) {
      engagementListSink.add(Response.error(e.toString()));
      print(e);
    }
  }

//  dispose() {
//    _chuckListController?.close();
//  }
}
