
import 'package:flutter/material.dart';
import 'package:rdcloseoutsession/StatefulWrapper.dart';
import 'package:rdcloseoutsession/engagement_cardcontainer.dart';
import 'package:rdcloseoutsession/list_engagement.dart';

class EngagementListContainer extends StatelessWidget {
  const EngagementListContainer({
    Key key,
    this.isTabletDevice = false,
    this.scrollPhysics,
    this.onItemChangeListener,
    this.totalCountListener,
    @required this.engagementList,
  }) : super(key: key);

  final ListEngagement engagementList;
  final bool isTabletDevice;
  final ScrollPhysics scrollPhysics;
  final ValueChanged<ListEngagement> onItemChangeListener;
  final ValueChanged<int> totalCountListener;

  @override
  Widget build(BuildContext context) {
    return StatefulWrapper(
      onInit: () {
        totalCountListener(engagementList.totalCount ?? 0);
      },
      child: Container(
        child: ListView.builder(
            shrinkWrap: true,
            physics: scrollPhysics ?? AlwaysScrollableScrollPhysics(),
            itemCount:
                engagementList == null ? 0 : engagementList.result.length,
            itemBuilder: (context, index) {
              return Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, right: 10.0),
                    child: GestureDetector(
                      onTap: () {
//                        Navigator.push(
//                            context,
//                            MaterialPageRoute(
//                                builder: (context) => EngagementDetailPage(
//                                    engagementList
//                                        .result[index].engagementType.type,
//                                    engagementList.result[index].id
//                                        .toString())));
                      },
                      child: EngagementCardContainer(
                        isTablet: isTabletDevice,
                        isEngagement: true,
                        engagement: engagementList.result[index],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 8.0,
                  )
                ],
              );
            }),
      ),
    );
  }
}
