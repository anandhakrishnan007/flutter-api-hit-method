/// Tree view widget library
library tree_view;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rdcloseoutsession/CommonUtils.dart';
import 'package:rdcloseoutsession/custom_app_theme.dart';
import 'package:rdcloseoutsession/network/SampleTest.dart';

typedef void ParentSelectChanged(bool isSelected);

/// # Tree View
///
/// Creates a tree view widget. The widget is a List View with a [List] of
/// [Parent] widgets. The [TreeView] is nested inside a [Scrollbar] if the
/// [TreeView.hasScrollBar] property is true.
class TreeView extends StatelessWidget {
  final List<Parent> parentList;
  final bool hasScrollBar;

  TreeView({
    this.parentList = const <Parent>[],
    this.hasScrollBar = true,
  });

  @override
  Widget build(BuildContext context) {
    return hasScrollBar ? Scrollbar(child: _getTreeList()) : _getTreeList();
  }

  Widget _getTreeList() {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        return parentList[index];
      },
      itemCount: parentList.length,
    );
  }
}

/// # Parent widget
///
/// The [Parent] widget holds the [Parent.parent] widget and
/// [Parent.childList] which is a [List] of child widgets.
///
/// The [Parent] widget is wrapped around a [Column]. The [Parent.childList]
/// is collapsed by default. When clicked the child widget is expanded.
///

class Parent extends StatefulWidget {
  final List<Parent> parentList;
  final Widget parent;
  final ChildList childList;
  final MainAxisSize mainAxisSize;
  final CrossAxisAlignment crossAxisAlignment;
  final MainAxisAlignment mainAxisAlignment;
  final ParentSelectChanged callback;
  final Key key;
  final Document documentdata;
  final bool isShowLeading;
  final bool showColor;

  Parent({
    @required this.parent,
    @required this.childList,
    @required this.documentdata,
    this.mainAxisAlignment = MainAxisAlignment.center,
    this.crossAxisAlignment = CrossAxisAlignment.start,
    this.mainAxisSize = MainAxisSize.min,
    this.callback,
    this.key,
    this.parentList,
    this.isShowLeading,
    this.showColor,
  });

  @override
  ParentState createState() => ParentState();
}

class ParentState extends State<Parent> {
  bool _isSelected = false;
  bool monVal = false;
  bool _value = false;

  void _arrowicon() {
    setState(() {
      _value = !_value;
      print(">>>>>>>>>>> $_value");
    });
  }

  @override
  Widget build(BuildContext context) {
    print(" margin values CustomTreeview ${widget.documentdata.docindex}");
    return Material(
      child: Column(
        mainAxisSize: widget.mainAxisSize,
        mainAxisAlignment: widget.mainAxisAlignment,
        children: <Widget>[
          Container(
            color: CustomAppTheme.bgGreyColor,
            child: Column(
              children: [
                Container(
                  color: widget.showColor
                      ? Colors.white
                      : CustomAppTheme.bgGreyColor,
                  padding: EdgeInsets.only(left: 15.0, right: 20.0),
                  child: GestureDetector(
                    child: Container(
                        margin: EdgeInsets.only(
                          left: widget.documentdata.docindex + 4,
                        ),
                        child: ListTile(
                            dense: true,
                            contentPadding: EdgeInsets.all(0),
                            leading: Visibility(
                              visible: widget.isShowLeading,
                              child: _value
                                  ? Icon(
                                      Icons.keyboard_arrow_down,
                                      color: Theme.of(context).primaryColor,
                                      size: 28,
                                    )
                                  : Icon(
                                      Icons.keyboard_arrow_right,
                                      color: Theme.of(context).primaryColor,
                                      size: 28,
                                    ),
                            ),
                            title: Transform.translate(
                              offset: Offset(-25.0, 0.0),
                              child: widget.parent,
                            ),
                            trailing: GestureDetector(
                              onTap: () {
                                widget.documentdata
                                    .onItemChanged(widget.documentdata);
                                setState(() {
                                  widget.documentdata.isChecked =
                                      !widget.documentdata.isChecked;
                                  monVal = widget.documentdata.isChecked;
                                });
                              },
                              child: Container(
                                child: widget.documentdata.isChecked
                                    ? new Image.asset(
                                        CommonUtils.assetsImage('checked'),
                                        width: 16,
                                        height: 16,
                                      )
                                    : new Image.asset(
                                        CommonUtils.assetsImage('un_checked'),
                                        width: 16,
                                        height: 16,
                                      ),
                              ),
                            )
//                Checkbox(
//                  value: widget.documentdata.isChecked,
//                  onChanged: (bool value) {
//                    widget.documentdata.onItemChanged(widget.documentdata);
//                    setState(() {
//                      monVal = value;
//                    });
//                  },
//                ),
                            )),
                    onTap: () {
                      expand();
                      _arrowicon();
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 45.0),
                  child: Divider(
                    height: 0.0,
                    thickness: 0.5,
                    color: Colors.black,
                  ),
                )
              ],
            ),
          ),
          _getChild()
        ],
      ),
    );
  }

  void expand() {
    if (widget.callback != null) widget.callback(_isSelected);
    setState(() {
      _isSelected = _toggleBool(_isSelected);
      print(_toggleBool);
    });
  }

  bool _toggleBool(bool b) {
    return b ? false : true;
  }

  Widget _getChild() {
    return _isSelected ? widget.childList : Container();
  }
}

/// # ChildList widget
///
/// The [ChildList] widget holds a [List] of widget which will be displayed as
/// children of the [Parent] widget
class ChildList extends StatelessWidget {
  final List<Widget> children;
  final MainAxisAlignment mainAxisAlignment;
  final CrossAxisAlignment crossAxisAlignment;
  final MainAxisSize mainAxisSize;

  ChildList({
    this.children = const <Widget>[],
    this.mainAxisSize = MainAxisSize.min,
    this.crossAxisAlignment = CrossAxisAlignment.start,
    this.mainAxisAlignment = MainAxisAlignment.center,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
//      color: Colors.grey,
      child: Column(
        mainAxisAlignment: mainAxisAlignment,
        crossAxisAlignment: crossAxisAlignment,
        mainAxisSize: mainAxisSize,
        children: children,
      ),
    );
  }
}
