import 'dart:convert';

import 'package:meta/meta.dart';

class GeoModel {
  final int statusCode;
  final bool apiStatus;
  final Result result;

  GeoModel({
    @required this.statusCode,
    @required this.apiStatus,
    @required this.result,
  });

  factory GeoModel.fromJson(String str) => GeoModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory GeoModel.fromMap(Map<String, dynamic> json) => GeoModel(
        statusCode: json["statusCode"] == null ? null : json["statusCode"],
        apiStatus: json["apiStatus"] == null ? null : json["apiStatus"],
        result: json["result"] == null ? null : Result.fromMap(json["result"]),
      );

  Map<String, dynamic> toMap() => {
        "statusCode": statusCode == null ? null : statusCode,
        "apiStatus": apiStatus == null ? null : apiStatus,
        "result": result == null ? null : result.toMap(),
      };
}

class Result {
  final List<Geo> geo;

  Result({
    @required this.geo,
  });

  factory Result.fromJson(String str) => Result.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Result.fromMap(Map<String, dynamic> json) => Result(
        geo: json["Geo"] == null
            ? null
            : List<Geo>.from(json["Geo"].map((x) => Geo.fromMap(x))),
      );

  Map<String, dynamic> toMap() => {
        "Geo":
            geo == null ? null : List<dynamic>.from(geo.map((x) => x.toMap())),
      };
}

class Geo {
  final Field field;
  final String fieldId;
  final String fieldName;
  final int id;
  final AtedAt createdAt;
  final AtedAt updatedAt;
  final List<Geo> geo1;
  final int geoadd;
  final List<Geo> geo2;
  final int geoadd1;
  final List<Geo> geo3;
  final int geoadd2;
  final List<RepUser> repUsers;
   bool isslected;

  Geo({
    @required this.field,
    @required this.fieldId,
    @required this.fieldName,
    @required this.id,
    @required this.createdAt,
    @required this.updatedAt,
    @required this.geo1,
    @required this.geoadd,
    @required this.geo2,
    @required this.geoadd1,
    @required this.geo3,
    @required this.geoadd2,
    @required this.repUsers,
    @required this.isslected,
  });

  factory Geo.fromJson(String str) => Geo.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Geo.fromMap(Map<String, dynamic> json) => Geo(
      field: json["field"] == null ? null : fieldValues.map[json["field"]],
      fieldId: json["field_id"] == null ? null : json["field_id"],
      fieldName: json["field_name"] == null ? null : json["field_name"],
      id: json["id"] == null ? null : json["id"],
      createdAt: json["createdAt"] == null
          ? null
          : atedAtValues.map[json["createdAt"]],
      updatedAt: json["updatedAt"] == null
          ? null
          : atedAtValues.map[json["updatedAt"]],
      geo1: json["Geo1"] == null
          ? null
          : List<Geo>.from(json["Geo1"].map((x) => Geo.fromMap(x))),
      geoadd: json["geoadd"] == null ? null : json["geoadd"],
      geo2: json["Geo2"] == null
          ? null
          : List<Geo>.from(json["Geo2"].map((x) => Geo.fromMap(x))),
      geoadd1: json["geoadd1"] == null ? null : json["geoadd1"],
      geo3: json["Geo3"] == null
          ? null
          : List<Geo>.from(json["Geo3"].map((x) => Geo.fromMap(x))),
      geoadd2: json["geoadd2"] == null ? null : json["geoadd2"],
      repUsers: json["repUsers"] == null
          ? null
          : List<RepUser>.from(json["repUsers"].map((x) => RepUser.fromMap(x))),
      isslected: false,);

  Map<String, dynamic> toMap() => {
        "field": field == null ? null : fieldValues.reverse[field],
        "field_id": fieldId == null ? null : fieldId,
        "field_name": fieldName == null ? null : fieldName,
        "id": id == null ? null : id,
        "createdAt": createdAt == null ? null : atedAtValues.reverse[createdAt],
        "updatedAt": updatedAt == null ? null : atedAtValues.reverse[updatedAt],
        "Geo1": geo1 == null
            ? null
            : List<dynamic>.from(geo1.map((x) => x.toMap())),
        "geoadd": geoadd == null ? null : geoadd,
        "Geo2": geo2 == null
            ? null
            : List<dynamic>.from(geo2.map((x) => x.toMap())),
        "geoadd1": geoadd1 == null ? null : geoadd1,
        "Geo3": geo3 == null
            ? null
            : List<dynamic>.from(geo3.map((x) => x.toMap())),
        "geoadd2": geoadd2 == null ? null : geoadd2,
        "repUsers": repUsers == null
            ? null
            : List<dynamic>.from(repUsers.map((x) => x.toMap())),
      };
}

enum AtedAt { THE_00000000000000 }

final atedAtValues =
    EnumValues({"0000-00-00 00:00:00": AtedAt.THE_00000000000000});

enum Field { NATIONAL, REGION, TERRITORY }

final fieldValues = EnumValues({
  "National": Field.NATIONAL,
  "Region": Field.REGION,
  "Territory": Field.TERRITORY
});

class RepUser {
  final List<dynamic> bookingToUser;
  final List<dynamic> bookingFromUser;
  final List<dynamic> meetingRequestToUser;
  final List<dynamic> meetingRequestFromUser;
  final List<dynamic> meeting;
  final List<dynamic> userBrand;
  final List<dynamic> userProduct;
  final List<dynamic> hierarchyDetails;
  final List<dynamic> userPhoto;
  final List<dynamic> contactInfo;
  final List<dynamic> practiceInfo;
  final List<dynamic> medicalInfo;
  final List<dynamic> territoryAlignment;
  final List<dynamic> userDocument;
  final List<dynamic> userComment;
  final List<dynamic> userTopic;
  final List<dynamic> spkfeeforservice;
  final List<dynamic> geoDetails;
  final List<dynamic> spkTravel;
  final List<dynamic> spkBank;
  final int id;
  final String firstName;
  final String lastName;
  final String email;
  final String telephone;
  final String state;
  final String userType;
  final String city;
  final Photo photo;
  final PhotoPath photoPath;
  final String degree;
  final dynamic speciality;
  final dynamic institution;
  final Area area;
  final Region region;
  final String district;
  final String territory;
  bool isselected;

  RepUser({
    @required this.bookingToUser,
    @required this.bookingFromUser,
    @required this.meetingRequestToUser,
    @required this.meetingRequestFromUser,
    @required this.meeting,
    @required this.userBrand,
    @required this.userProduct,
    @required this.hierarchyDetails,
    @required this.userPhoto,
    @required this.contactInfo,
    @required this.practiceInfo,
    @required this.medicalInfo,
    @required this.territoryAlignment,
    @required this.userDocument,
    @required this.userComment,
    @required this.userTopic,
    @required this.spkfeeforservice,
    @required this.geoDetails,
    @required this.spkTravel,
    @required this.spkBank,
    @required this.id,
    @required this.firstName,
    @required this.lastName,
    @required this.email,
    @required this.telephone,
    @required this.state,
    @required this.userType,
    @required this.city,
    @required this.photo,
    @required this.photoPath,
    @required this.degree,
    @required this.speciality,
    @required this.institution,
    @required this.area,
    @required this.region,
    @required this.district,
    @required this.territory,
    this.isselected
  });

  factory RepUser.fromJson(String str) => RepUser.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory RepUser.fromMap(Map<String, dynamic> json) => RepUser(
        bookingToUser: json["bookingToUser"] == null
            ? null
            : List<dynamic>.from(json["bookingToUser"].map((x) => x)),
        bookingFromUser: json["bookingFromUser"] == null
            ? null
            : List<dynamic>.from(json["bookingFromUser"].map((x) => x)),
        meetingRequestToUser: json["meetingRequestToUser"] == null
            ? null
            : List<dynamic>.from(json["meetingRequestToUser"].map((x) => x)),
        meetingRequestFromUser: json["meetingRequestFromUser"] == null
            ? null
            : List<dynamic>.from(json["meetingRequestFromUser"].map((x) => x)),
        meeting: json["meeting"] == null
            ? null
            : List<dynamic>.from(json["meeting"].map((x) => x)),
        userBrand: json["userBrand"] == null
            ? null
            : List<dynamic>.from(json["userBrand"].map((x) => x)),
        userProduct: json["userProduct"] == null
            ? null
            : List<dynamic>.from(json["userProduct"].map((x) => x)),
        hierarchyDetails: json["hierarchyDetails"] == null
            ? null
            : List<dynamic>.from(json["hierarchyDetails"].map((x) => x)),
        userPhoto: json["userPhoto"] == null
            ? null
            : List<dynamic>.from(json["userPhoto"].map((x) => x)),
        contactInfo: json["contactInfo"] == null
            ? null
            : List<dynamic>.from(json["contactInfo"].map((x) => x)),
        practiceInfo: json["practiceInfo"] == null
            ? null
            : List<dynamic>.from(json["practiceInfo"].map((x) => x)),
        medicalInfo: json["medicalInfo"] == null
            ? null
            : List<dynamic>.from(json["medicalInfo"].map((x) => x)),
        territoryAlignment: json["territoryAlignment"] == null
            ? null
            : List<dynamic>.from(json["territoryAlignment"].map((x) => x)),
        userDocument: json["userDocument"] == null
            ? null
            : List<dynamic>.from(json["userDocument"].map((x) => x)),
        userComment: json["userComment"] == null
            ? null
            : List<dynamic>.from(json["userComment"].map((x) => x)),
        userTopic: json["userTopic"] == null
            ? null
            : List<dynamic>.from(json["userTopic"].map((x) => x)),
        spkfeeforservice: json["spkfeeforservice"] == null
            ? null
            : List<dynamic>.from(json["spkfeeforservice"].map((x) => x)),
        geoDetails: json["geoDetails"] == null
            ? null
            : List<dynamic>.from(json["geoDetails"].map((x) => x)),
        spkTravel: json["SpkTravel"] == null
            ? null
            : List<dynamic>.from(json["SpkTravel"].map((x) => x)),
        spkBank: json["SpkBank"] == null
            ? null
            : List<dynamic>.from(json["SpkBank"].map((x) => x)),
        id: json["id"] == null ? null : json["id"],
        firstName: json["firstName"] == null ? null : json["firstName"],
        lastName: json["lastName"] == null ? null : json["lastName"],
        email: json["email"] == null ? null : json["email"],
        telephone: json["telephone"] == null ? null : json["telephone"],
        state: json["state"] == null ? null : json["state"],
        userType: json["userType"] == null ? null : json["userType"],
        city: json["city"] == null ? null : json["city"],
        photo: json["photo"] == null ? null : photoValues.map[json["photo"]],
        photoPath: json["photo_path"] == null
            ? null
            : photoPathValues.map[json["photo_path"]],
        degree: json["degree"] == null ? null : json["degree"],
        speciality: json["speciality"],
        institution: json["institution"],
        area: json["area"] == null ? null : areaValues.map[json["area"]],
        region:
            json["region"] == null ? null : regionValues.map[json["region"]],
        district: json["district"] == null ? null : json["district"],
        territory: json["territory"] == null ? null : json["territory"],
      isselected:false,
      );

  Map<String, dynamic> toMap() => {
        "bookingToUser": bookingToUser == null
            ? null
            : List<dynamic>.from(bookingToUser.map((x) => x)),
        "bookingFromUser": bookingFromUser == null
            ? null
            : List<dynamic>.from(bookingFromUser.map((x) => x)),
        "meetingRequestToUser": meetingRequestToUser == null
            ? null
            : List<dynamic>.from(meetingRequestToUser.map((x) => x)),
        "meetingRequestFromUser": meetingRequestFromUser == null
            ? null
            : List<dynamic>.from(meetingRequestFromUser.map((x) => x)),
        "meeting":
            meeting == null ? null : List<dynamic>.from(meeting.map((x) => x)),
        "userBrand": userBrand == null
            ? null
            : List<dynamic>.from(userBrand.map((x) => x)),
        "userProduct": userProduct == null
            ? null
            : List<dynamic>.from(userProduct.map((x) => x)),
        "hierarchyDetails": hierarchyDetails == null
            ? null
            : List<dynamic>.from(hierarchyDetails.map((x) => x)),
        "userPhoto": userPhoto == null
            ? null
            : List<dynamic>.from(userPhoto.map((x) => x)),
        "contactInfo": contactInfo == null
            ? null
            : List<dynamic>.from(contactInfo.map((x) => x)),
        "practiceInfo": practiceInfo == null
            ? null
            : List<dynamic>.from(practiceInfo.map((x) => x)),
        "medicalInfo": medicalInfo == null
            ? null
            : List<dynamic>.from(medicalInfo.map((x) => x)),
        "territoryAlignment": territoryAlignment == null
            ? null
            : List<dynamic>.from(territoryAlignment.map((x) => x)),
        "userDocument": userDocument == null
            ? null
            : List<dynamic>.from(userDocument.map((x) => x)),
        "userComment": userComment == null
            ? null
            : List<dynamic>.from(userComment.map((x) => x)),
        "userTopic": userTopic == null
            ? null
            : List<dynamic>.from(userTopic.map((x) => x)),
        "spkfeeforservice": spkfeeforservice == null
            ? null
            : List<dynamic>.from(spkfeeforservice.map((x) => x)),
        "geoDetails": geoDetails == null
            ? null
            : List<dynamic>.from(geoDetails.map((x) => x)),
        "SpkTravel": spkTravel == null
            ? null
            : List<dynamic>.from(spkTravel.map((x) => x)),
        "SpkBank":
            spkBank == null ? null : List<dynamic>.from(spkBank.map((x) => x)),
        "id": id == null ? null : id,
        "firstName": firstName == null ? null : firstName,
        "lastName": lastName == null ? null : lastName,
        "email": email == null ? null : email,
        "telephone": telephone == null ? null : telephone,
        "state": state == null ? null : state,
        "userType": userType == null ? null : userType,
        "city": city == null ? null : city,
        "photo": photo == null ? null : photoValues.reverse[photo],
        "photo_path":
            photoPath == null ? null : photoPathValues.reverse[photoPath],
        "degree": degree == null ? null : degree,
        "speciality": speciality,
        "institution": institution,
        "area": area == null ? null : areaValues.reverse[area],
        "region": region == null ? null : regionValues.reverse[region],
        "district": district == null ? null : district,
        "territory": territory == null ? null : territory,
      };
}

enum Area { K0100, K0200, K0300, K0400 }

final areaValues = EnumValues({
  "K0100": Area.K0100,
  "K0200": Area.K0200,
  "K0300": Area.K0300,
  "K0400": Area.K0400
});

enum Photo {
  EMPTY,
  THE_8_FE08_C0_F_7_FFA_4_E20_81_FA_4_CAB81239_C05_JPG,
  THE_6202_A80_D_39_D5_4_E13_B307_339900_F7_F671_JPEG
}

final photoValues = EnumValues({
  "": Photo.EMPTY,
  "/6202a80d-39d5-4e13-b307-339900f7f671.jpeg":
      Photo.THE_6202_A80_D_39_D5_4_E13_B307_339900_F7_F671_JPEG,
  "/8fe08c0f-7ffa-4e20-81fa-4cab81239c05.jpg":
      Photo.THE_8_FE08_C0_F_7_FFA_4_E20_81_FA_4_CAB81239_C05_JPG
});

enum PhotoPath { EMPTY, IMAGES_PROFILEIMAGE }

final photoPathValues = EnumValues({
  "": PhotoPath.EMPTY,
  "/images/profileimage": PhotoPath.IMAGES_PROFILEIMAGE
});

enum Region { NORTHEAST, SOUTH, CENTRAL, WEST }

final regionValues = EnumValues({
  "CENTRAL": Region.CENTRAL,
  "NORTHEAST": Region.NORTHEAST,
  "SOUTH": Region.SOUTH,
  "WEST": Region.WEST
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
