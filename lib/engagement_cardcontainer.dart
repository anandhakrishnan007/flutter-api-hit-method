import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:intl/intl.dart';
import 'package:rdcloseoutsession/StatefulWrapper.dart';
import 'package:rdcloseoutsession/WebViewStream.dart';
import 'package:rdcloseoutsession/list_engagement.dart';

class EngagementCardContainer extends StatelessWidget {
  const EngagementCardContainer({
    Key key,
    this.engagement,
    this.slidableController,
    this.isTablet = false,
    this.isApproval = false,
    this.isEngagement = false,
    this.isDragSlidable = false,
    this.onDeclineTap,
    this.onApproveTap,
    this.isCollaborations = false,
  }) : super(key: key);

  final HomeResult engagement;
  final bool isTablet;

  final bool isApproval;
  final bool isEngagement;
  final bool isCollaborations;
  final bool isDragSlidable;
  final SlidableController slidableController;
  final Function onDeclineTap;
  final Function onApproveTap;

  @override
  Widget build(BuildContext context) {
    return StatefulWrapper(
      onInit: () {
        dateTimeFormat(engagement?.startTime);
      },
      child: Slidable(
        key: Key(engagement?.id.toString()),
        enabled: isDragSlidable,
        controller: slidableController,
        actionExtentRatio: isTablet ? 0.20 : 0.25,
        actionPane: SlidableDrawerActionPane(),
        secondaryActions: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 5.0),
            child: IconSlideAction(
              iconWidget: Container(
                decoration: BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Flexible(
                        child: new Icon(
                          Icons.clear,
                          color: Colors.white,
                        ),
                      ),
                      Flexible(
                        child: Text(
                          'Decline',
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context)
                              .primaryTextTheme
                              .caption
                              .copyWith(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                fontSize: 16.0,
                              ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              onTap: onDeclineTap,
              color: Colors.transparent,
            ),
          ),
        ],
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 5.0),
            child: IconSlideAction(
              iconWidget: Container(
                decoration: BoxDecoration(
                  color: Colors.green,
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Flexible(
                        child: new Icon(
                          Icons.check,
                          color: Colors.white,
                        ),
                      ),
                      Flexible(
                        child: Text(
                          'Approve',
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context)
                              .primaryTextTheme
                              .caption
                              .copyWith(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                fontSize: 16.0,
                              ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              onTap: onApproveTap,
              color: Colors.transparent,
            ),
          ),
        ],
        child: ClipPath(
          clipper: ShapeBorderClipper(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10),
                topRight: Radius.circular(10),
                bottomLeft: Radius.circular(10),
                bottomRight: Radius.circular(10),
              ),
            ),
          ),
          child: Container(
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border(
                left: BorderSide(
                  width: 8.0,
                  color: engagement?.engagementColor != 0
                      ? Color(engagement?.engagementColor)
                      : Theme.of(context).buttonColor,
                ),
              ),
            ),
            child: Padding(
              padding: EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Text(
                        'ID: ${engagement?.id ?? ''}',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: isTablet ? 16.0 : 14.0,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
//                      Visibility(
//                        visible: isEngagement,
//                        child: Padding(
//                          padding: EdgeInsets.only(left: 4.0, right: 4.0),
//                          child: Image.asset(
//                            CommonUtils.assetsImage(
//                                engagement?.engagementType.type),
//                            height: 16,
//                            width: 16,
//                            fit: BoxFit.contain,
//                          ),
//                        ),
//                      ),
                      Flexible(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Flexible(
                              child: RoundedTextContainer(
                                text: engagement?.engagementType.type,
                                outlineColor: engagement?.engagementColor != 0
                                    ? Color(engagement?.engagementColor)
                                    : Theme.of(context).buttonColor,
                                isTablet: isTablet,
                              ),
                            ),
                            Visibility(
                              visible: isEngagement,
                              child: GestureDetector(
                                onTap: () {
//                                  Navigator.push(
//                                    context,
//                                    MaterialPageRoute(
//                                      builder: (context) =>
//                                          EngagementDetailPage(
//                                              engagement.engagementType.type,
//                                              engagement.id.toString()),
//                                    ),
//                                  );

                                  print('clicked card >>>');
                                },
                                child: RoundedTextContainer(
                                  text: engagement?.displayStatus.toString(),
                                  isTablet: isTablet,
                                  outlineColor: Theme.of(context).buttonColor,
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 6, bottom: 6),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          engagement?.topic,
                          style: TextStyle(
                              fontSize: isTablet ? 16.0 : 14.0,
                              fontWeight: FontWeight.w500),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 5, bottom: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                engagement?.brand.brandName,
                                style: TextStyle(
                                    color: Theme.of(context)
                                        .textTheme
                                        .caption
                                        .color,
                                    fontSize: isTablet ? 16.0 : 14.0,
                                    fontWeight: FontWeight.w100),
                              ),
                              Icon(
                                Icons.arrow_forward_ios,
                                size: 14,
                                color: Theme.of(context).buttonColor,
                              ),
                              Flexible(
                                child: Html(
                                  data: engagement?.product.productName,
                                  useRichText: false,
                                  //Optional parameters:
                                  defaultTextStyle: TextStyle(
                                      color: Theme.of(context)
                                          .textTheme
                                          .caption
                                          .color,
                                      fontSize: isTablet ? 16.0 : 14.0,
                                      fontWeight: FontWeight.w100),
                                  linkStyle: TextStyle(
                                    color: Colors.red,
                                    fontSize: isTablet ? 16.0 : 14.0,
                                    fontWeight: FontWeight.w400,
                                  ),
                                  onLinkTap: (url) {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => WebViewStream(
                                                title: '', selectedUrl: url)));
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                        Divider(
                          color: Colors.grey,
                          height: 0.0,
                        ),
                      ],
                    ),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Visibility(
                              visible: isApproval || isCollaborations,
                              child: Text(
                                'EO: ${engagement?.user.firstName != null && engagement?.user.firstName != '' ? engagement?.user.firstName : '---' ?? '---'}',
                                style: TextStyle(
                                    fontSize: isTablet ? 16.0 : 14.0,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                            Visibility(
                              visible: isEngagement || isCollaborations,
                              child: Text(
                                'Speaker: ${engagement?.speaker.first != null && engagement?.speaker.first != '' ? engagement?.speaker.first : '---' ?? '---'}',
                                style: TextStyle(
                                    fontSize: isTablet ? 16.0 : 14.0,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                            Visibility(
                              visible: isEngagement,
                              child: Text(
                                'Invitees: ${engagement?.invited ?? '0'}',
                                style: TextStyle(
                                    fontSize: isTablet ? 16.0 : 14.0,
                                    color: Theme.of(context).buttonColor,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                            Visibility(
                              visible: isApproval,
                              child: Text(
                                'Attendee(s) Invited: ${engagement?.invited ?? '0'}',
                                style: TextStyle(
                                    fontSize: isTablet ? 16.0 : 14.0,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                            Visibility(
                              visible: isEngagement,
                              child: Text(
                                'Attendees Confirmed: ${engagement?.confirmedInvitees ?? '0'}',
                                style: TextStyle(
                                    fontSize: isTablet ? 16.0 : 14.0,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Text(
                          dateTimeFormat(engagement?.startTime),
                          style: TextStyle(
                              fontSize: isTablet ? 16.0 : 14.0,
                              color: Theme.of(context).textTheme.caption.color,
                              fontWeight: FontWeight.w100),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  String dateTimeFormat(DateTime startime) {
    DateTime strDate = startime.toLocal();

    var dateFormat = DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", 'en_US');

    DateTime date = null;

    try {
      String formattedDate = dateFormat.format(strDate);
      date = dateFormat.parse(formattedDate);

      var formatter = DateFormat("MMM dd, yyyy", 'en_US');
      String dateStr = formatter.format(date);

      var timeformatter = DateFormat("hh.mm a", 'en_US');
      String time = timeformatter.format(date);

      return dateStr + "\n" + time;
    } catch (e) {
      e.printStackTrace();
    }
  }
}

class RoundedTextContainer extends StatelessWidget {
  const RoundedTextContainer(
      {Key key,
      @required this.text,
      this.marginEdgeInsets,
      this.paddingEdgeInsets,
      this.outlineColor,
      this.circleRadius = 4.0,
      this.removeOutLine = false,
      this.isTablet = false,
      this.textStyle})
      : super(key: key);

  final String text;
  final EdgeInsetsGeometry marginEdgeInsets;
  final EdgeInsetsGeometry paddingEdgeInsets;
  final Color outlineColor;
  final double circleRadius;
  final TextStyle textStyle;
  final bool isTablet;
  final bool removeOutLine;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: marginEdgeInsets ?? EdgeInsets.only(left: 8),
      padding: paddingEdgeInsets ??
          EdgeInsets.only(left: 8, top: 4, bottom: 4, right: 8),
      decoration: removeOutLine
          ? null
          : BoxDecoration(
              border: Border.all(
                color: outlineColor ?? Theme.of(context).buttonColor,
                width: 1,
              ),
              borderRadius: BorderRadius.circular(circleRadius ?? 4.0),
            ),
      child: Text(
        text ?? '',
        maxLines: 1,
        overflow: TextOverflow.fade,
        softWrap: false,
        style: textStyle ?? TextStyle(fontSize: isTablet ? 16.0 : 14.0),
      ),
    );
  }
}
