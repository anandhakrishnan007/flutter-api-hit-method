import 'package:json_annotation/json_annotation.dart';


@JsonSerializable()
class engagementmodel {
  num statusCode;
  bool apiStatus;
  num totalCount;
  List<ResultBean> result;

  engagementmodel({this.statusCode, this.apiStatus, this.totalCount, this.result});

  factory engagementmodel.fromJson(Map<String, dynamic> json) => engagementmodel.fromJson(json);

 
}

@JsonSerializable()
class ResultBean {
  EngagementTypeBean engagementType;
  num id;
  UserBean user;
  String topic;
  int start_time;
  String meeting_id;
  BrandBean brand;
  ProductBean product;
  LocationBean location;
  num status;
  num venue_status;
  num is_start;
  String venue_selected_timezone;
  String eo_selected_timezone;
  String displayStatus;
  dynamic primaryMeeting;
  List<String> speaker;
  String collaboration_meeting;
  num invited;
  num confirmed_invitees;
  dynamic hcpOrRep;
  ParticipantStatusBean participantStatus;

  ResultBean({this.engagementType, this.id, this.user, this.topic, this.start_time, this.meeting_id, this.brand, this.product, this.location, this.status, this.venue_status, this.is_start, this.venue_selected_timezone, this.eo_selected_timezone, this.displayStatus, this.primaryMeeting, this.speaker, this.collaboration_meeting, this.invited, this.confirmed_invitees, this.hcpOrRep, this.participantStatus});

  factory ResultBean.fromJson(Map<String, dynamic> json) => ResultBean.fromJson(json);

  //Map<String, dynamic> toJson() => _$ResultBeanToJson(this);
}

@JsonSerializable()
class ParticipantStatusBean {
  num participant;
  num userType;
  num status;
  num meeting;
  num waiting_status;
  num pause_status;
  num leave_status;
  dynamic sessionId;
  dynamic token;
  dynamic closeOutBy;
  num attendees_close_out_method;
  num sign_in_status;
  dynamic no_show_comments;
  dynamic sign_in_image;
  dynamic sign_in_image_path;
  num opt_out_meal;
  dynamic opt_lic_state;
  num ffs;
  String session_mins;
  num id;
  int createdAt;
  int updatedAt;

  ParticipantStatusBean({this.participant, this.userType, this.status, this.meeting, this.waiting_status, this.pause_status, this.leave_status, this.sessionId, this.token, this.closeOutBy, this.attendees_close_out_method, this.sign_in_status, this.no_show_comments, this.sign_in_image, this.sign_in_image_path, this.opt_out_meal, this.opt_lic_state, this.ffs, this.session_mins, this.id, this.createdAt, this.updatedAt});

  factory ParticipantStatusBean.fromJson(Map<String, dynamic> json) => ParticipantStatusBean.fromJson(json);

  //Map<String, dynamic> toJson() => _$ParticipantStatusBeanToJson(this);
}

@JsonSerializable()
class LocationBean {
  String location_name;
  String location_address;
  String location_city;
  String location_state;
  String location_zip;
  String location_phone;
  num location_amount;
  num id;
  String createdAt;
  String updatedAt;
  num engagementType;

  LocationBean({this.location_name, this.location_address, this.location_city, this.location_state, this.location_zip, this.location_phone, this.location_amount, this.id, this.createdAt, this.updatedAt, this.engagementType});

  factory LocationBean.fromJson(Map<String, dynamic> json) => LocationBean.fromJson(json);

  //Map<String, dynamic> toJson() => _$LocationBeanToJson(this);
}

@JsonSerializable()
class ProductBean {
  String product_name;
  String product;
  String product_desc;
  String product_isi;
  num active_status;
  num delete_status;
  num created_by;
  num updated_by;
  num id;
  int createdAt;
  int updatedAt;
  num brand;
  num company;

  ProductBean({this.product_name, this.product, this.product_desc, this.product_isi, this.active_status, this.delete_status, this.created_by, this.updated_by, this.id, this.createdAt, this.updatedAt, this.brand, this.company});

  factory ProductBean.fromJson(Map<String, dynamic> json) => ProductBean.fromJson(json);

  //Map<String, dynamic> toJson() => _$ProductBeanToJson(this);
}

@JsonSerializable()
class BrandBean {
  String brand_name;
  String brand_image;
  String upload_path;
  String original_brand_image;
  num active_status;
  num delete_status;
  num created_by;
  num updated_by;
  num id;
  int createdAt;
  int updatedAt;
  num company;

  BrandBean({this.brand_name, this.brand_image, this.upload_path, this.original_brand_image, this.active_status, this.delete_status, this.created_by, this.updated_by, this.id, this.createdAt, this.updatedAt, this.company});

  factory BrandBean.fromJson(Map<String, dynamic> json) => BrandBean.fromJson(json);

  //Map<String, dynamic> toJson() => _$BrandBeanToJson(this);
}

@JsonSerializable()
class UserBean {
  String firstName;
  String lastName;
  String midName;
  String address2;
  String affiliation;
  String govt_employee;
  String licensed_state;
  String fullName;
  String preferred_name;
  String email;
  String home_number;
  String telephone;
  dynamic preferred_telephone;
  String degree;
  dynamic speciality;
  dynamic institution;
  String address;
  String city;
  String state;
  String zip;
  num prescriber;
  String area;
  String region;
  String district;
  String territory;
  dynamic yrsOfExperience;
  String biography;
  String biography_path;
  String cv;
  String cv_path;
  String photo;
  String photo_path;
  dynamic publication;
  dynamic institution_address;
  dynamic institution_address1;
  dynamic institution_state;
  dynamic institution_zip;
  dynamic institution_restriction;
  dynamic primary_email;
  dynamic secondry_email;
  dynamic state_of_license;
  dynamic npi;
  dynamic decile;
  String designation;
  dynamic companyName;
  String userType;
  num attendeeType;
  num no_of_liveprograms;
  num cap_amount;
  num cap_max_limit;
  num speaker_status;
  num active_status;
  num delete_status;
  String deviceToken;
  String encrypted_token_reset;
  num terms_condition;
  num reset_password_flag;
  num subscribe_to_newsletter;
  num intro_video;
  num created_by;
  num updated_by;
  num waiting_status;
  dynamic webinar_only;
  dynamic patient_user;
  dynamic stripeId;
  String employee_id;
  dynamic imed_id;
  dynamic hire_date;
  dynamic storage_facility_name;
  dynamic storage_phone;
  dynamic storage_address;
  dynamic storage_unit;
  dynamic storage_city;
  dynamic storage_state;
  dynamic storage_zip;
  dynamic prod_bags;
  dynamic last_updated;
  dynamic sales_force_note;
  num tier;
  num login_flag;
  num id;
  String createdAt;
  int updatedAt;
  dynamic rep;
  num dm;
  dynamic topic;
  num company;
  num role;

  UserBean({this.firstName, this.lastName, this.midName, this.address2, this.affiliation, this.govt_employee, this.licensed_state, this.fullName, this.preferred_name, this.email, this.home_number, this.telephone, this.preferred_telephone, this.degree, this.speciality, this.institution, this.address, this.city, this.state, this.zip, this.prescriber, this.area, this.region, this.district, this.territory, this.yrsOfExperience, this.biography, this.biography_path, this.cv, this.cv_path, this.photo, this.photo_path, this.publication, this.institution_address, this.institution_address1, this.institution_state, this.institution_zip, this.institution_restriction, this.primary_email, this.secondry_email, this.state_of_license, this.npi, this.decile, this.designation, this.companyName, this.userType, this.attendeeType, this.no_of_liveprograms, this.cap_amount, this.cap_max_limit, this.speaker_status, this.active_status, this.delete_status, this.deviceToken, this.encrypted_token_reset, this.terms_condition, this.reset_password_flag, this.subscribe_to_newsletter, this.intro_video, this.created_by, this.updated_by, this.waiting_status, this.webinar_only, this.patient_user, this.stripeId, this.employee_id, this.imed_id, this.hire_date, this.storage_facility_name, this.storage_phone, this.storage_address, this.storage_unit, this.storage_city, this.storage_state, this.storage_zip, this.prod_bags, this.last_updated, this.sales_force_note, this.tier, this.login_flag, this.id, this.createdAt, this.updatedAt, this.rep, this.dm, this.topic, this.company, this.role});

  factory UserBean.fromJson(Map<String, dynamic> json) => UserBean.fromJson(json);

  //Map<String, dynamic> toJson() => _$UserBeanToJson(this);
}

@JsonSerializable()
class EngagementTypeBean {
  String type;
  String description;
  num order_no;
  num is_start;
  num minimum_hcp;
  num prescriber_hcp;
  num is_collaborator;
  num is_cohost;
  num is_detail;
  num is_content_note;
  num is_question;
  num amount;
  num hcp_amount;
  num active_status;
  num delete_status;
  num id;
  int createdAt;
  int updatedAt;

  EngagementTypeBean({this.type, this.description, this.order_no, this.is_start, this.minimum_hcp, this.prescriber_hcp, this.is_collaborator, this.is_cohost, this.is_detail, this.is_content_note, this.is_question, this.amount, this.hcp_amount, this.active_status, this.delete_status, this.id, this.createdAt, this.updatedAt});

  factory EngagementTypeBean.fromJson(Map<String, dynamic> json) => EngagementTypeBean.fromJson(json);

  //Map<String, dynamic> toJson() => _$EngagementTypeBeanToJson(this);
}

