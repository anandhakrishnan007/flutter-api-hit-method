import 'package:flutter/material.dart';

class TabItemContainer extends StatelessWidget {
  final String title;
  final String count;
  final Color colorCode;
  final bool isSelected;
  final String mode;
  final TextStyle textStyle;
  final FontWeight fontWeight;
  final bool isTablet;

  TabItemContainer({
    this.title,
    this.count,
    this.colorCode,
    this.isSelected,
    this.mode,
    this.textStyle,
    this.fontWeight,
    this.isTablet = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: colorCode,
        borderRadius: BorderRadius.all(
          Radius.circular(5.0),
        ),
      ),
      margin: EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            title ?? '',
            style: textStyle ??
                TextStyle(
                  fontSize: isTablet ? 16.0 : 14.0,
                  color: mode != null && mode != '' && mode == 'Date'
                      ? isSelected ? Colors.white : Colors.black
                      : Colors.white,
                  fontWeight: fontWeight ?? FontWeight.w600,
                ),
          ),
          Visibility(
            visible: count != null && count != '',
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  count ?? '',
                  style: TextStyle(
                      fontSize: isTablet ? 20.0 : 18.0,
                      fontWeight: isSelected ? FontWeight.bold : FontWeight.normal,
                      color: Colors.white),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
