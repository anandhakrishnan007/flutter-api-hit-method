class CommonUtils {
  static bool _isDialogShowing = false;
  static String assetsPath = 'assets/images/';

  static String assetsImage(String fileName) {
    switch (fileName) {
      case 'Filter':
        return '${assetsPath}ic_filter_default.png';
      case 'arrow_down':
        return '${assetsPath}ic_arrow_down.png';
      case 'un_checked':
        return '${assetsPath}ic_unchecked_checkbox.png';
      case 'checked':
        return '${assetsPath}ic_checked_checkbox.png';
    }

    return '';
  }
}
